#!/usr/bin/env python
"""Sensor analysis classes

XXX DOC

INTERNAL UNITS: 
    * [L]: mm


jorge.duarte.campderros@cern.ch CERN/IFCA 2018.11.13
"""
__author__ = "Jordi Duarte-Campderros"
__credits__ = ["Jordi Duarte-Campderros"]
__version__ = "0.1"
__maintainer__ = "Jordi Duarte-Campderros"
__email__ = "jorge.duarte.campderros@cern.ch"
__status__ = "Development"

import os
import ROOT
import numpy as np
import numpy.ma as ma
import tables
import array
from extra_functions import automatic_moyal_fit, plot_histo
# -- FIXME Problems with --help
# from sifca_utils import plotting as pl

# Internal UNITS: 
# * [L]: mm
MM=1.0
UM = 1e-3*MM

def closest_measured_hit(sensor_id,point,tree):
    """Given a point, find the closest point extracted from the tree based
    on the euclidean distance. The `tree` TTree must contain the datamembers
    `sensorId`, `xPos` and `yPos`, all array-like (ROOT.vector or np.array)
    
    Parameters
    ----------
    sensor_id: int
            The DUT ID
    point: (float,float)
           The point at the DUT plane: (x,y)
    tree: ROOT.TTree
           A tree to extract the list of points to compare with `point`
    
    Return
    ------
    int: The index of the closest measured hit in the meas_tree
    """
    dut_hit_id_list = filter(lambda i: tree.sensorId[i] == sensor_id,xrange(len(tree.xPos)))
    assert len(dut_hit_id_list) != 0, \
            "Why there is not a measured hit, when in the pre-processing, the track found an associated hit?\n"\
            "Probably you mixed up the sensor plane and the z-ordered plane."+\
            ' Sensor:{} Point at sensor plane:{}'.format(sensor_id,point)
    return min(map(lambda i: (np.sqrt((tree.xPos[i]-point[0])**2.0+\
            (tree.yPos[i]-point[1])**2.0),i),dut_hit_id_list))[1]

    
# XXX === SLOWER 
#def np_closest_measured_hit(sensor_id,predpoint,meas_tree):
#    """ Find the closest measured hit to the fitted one, based
#    in the euclidean distance
#    
#    Parameters
#    ----------
#    sensor_id: int
#            The DUT ID
#    predpoint: (float,float)
#           The predicted point at the DUT (sensor_id) plane: (x,y)
#    meas_tree: ROOT.TTree
#           The measured hits in the DUT plane tree
#    
#    Return
#    ------
#    int: The index of the closest measured hit in the meas_tree
#    """
#    mask_meas = np.where(np.array(meas_tree.sensorId) == sensor_id)[0]
#    assert len(mask_meas) != 0, \
#            "Why is there is not a measured hit as a track found an associated hit?\n"\
#            "Probably you mixed up the sensor plane and the z-ordered plane."
#    # Build a [ [ x_i, y_i ], [ x_j, y_j ], ... ] with the measured hits
#    measpoints = np.array(zip(np.array(meas_tree.xPos.data())[mask_meas],np.array(meas_tree.yPos.data())[mask_meas]))
#    # Just return the index of the minimum norm from measpoints,
#    # therefore the index is related with the index of the mask, which contains the absolute index
#    indx_min = np.argmin(np.linalg.norm(predpoint-measpoints))
#    # And extract the absolute index from the mask 
#    return mask_meas[indx_min]

def convert_100by25_3D(mask):
    """Maps the chip channel into the sensory layout channel, whenever 
    the layout is 20x100. But this has to be checked by the user

    Parameter
    --------
    mask: numpy.ndarray

    Return
    ------
    numpy.ndarray
    """
    nm = np.zeros((int(mask.shape[0]*2),int(mask.shape[1]/2)))
    # Mapping ROC -> sensor
    col = lambda col,row: int(np.floor(col/2))
    row = lambda col,row: 2*row+(col%2)
    for r,valarray in enumerate(mask):
        for c,val in enumerate(valarray):
            nm[row(c,r)][col(c,r)] = val
    return nm


class dut_analysis(object):
    """Post-processing analysis class. Creates or reads a file
    processed with eutelizer
    """
    # Defining the relevant points in the quarter of cell, to
    # compare with the 
    ADREM_POINTS = { '50x50' : UM*np.array([ [5,5],
                        [12.5,12.5],
                        [20,20],
                        [5,20] ]),
                    '25x100': UM*np.array([ [5,1.25],
                        [25,6.25],
                        [45,11.25], 
                        [5,11.25],
                        [45,1.25] ])
                    }
    
    def __init__(self,thefile,isreader,no_check_stats=False,**kwd):
        """Creates a reader or writer. The Writer creates a ROOT file
        with the post-processing data performed by the eutelizer pos_proc.
        The Reader reads the ROOT file created by the writer
        

        Parameters
        ----------
        isreader: bool
            Whether the reader (True) or the writer (False) should be called
        kwd: keywords arguments to be feed to the Writer/Reader (see __init_writer 
        and __init_reader)
        """
        # Allow to store histograms with low statistical content 
        # (useful to merge afterwards with other files)
        if no_check_stats:
            self.min_bin_content = 0
        else:
            self.min_bin_content = 70

        if isreader:
            self.__init_reader(inputfile=thefile,**kwd)
        else:
            self.__init__writer(outputfile=thefile,fe=kwd['fe'],
                    calibration_active=kwd['calibration_active'],
                    do_noise_analysis=kwd['do_noise_analysis'],
                    tdac_mask_file=kwd['tdac_mask_file'])
        
        # if there is a drift correction applied, in order to be included
        # in the metadata
        self._dx_drift_applied = None
        self._dy_drift_applied = None
        if kwd.has_key('drift_x') and kwd['drift_x'] is not None:
            self._dx_drift_applied = tuple(kwd['drift_x'])
        if kwd.has_key('drift_y') and kwd['drift_y'] is not None:
            self._dy_drift_applied = tuple(kwd['drift_y'])
        
    def __init_reader(self,inputfile,**args):
        """Reader to deal with the output of the writer. Useful class
        to do final plotting, obtain results, ...

        Parameters
        ----------
        args: Namespace
            Arguments containing metadata for the plot, some to be printed in
            the plot, and usually provided by the main script.

            * format: str
                The format of the output [pdf| png| ..]
            * name_sensor: str
                The sensor name
            * name_layout: str
                The sensor layout [25x100|50x50]
            * voltage_bias: float|str
                The applied voltage bias
            * leakage_current: float|str
                The measured current
            * temperature: float|str
                The measured temperature
            * fluence: float|str
                The target fluence
        """
        self.__is_reader = True
        # Get file 
        self.root_file = ROOT.TFile(inputfile)
        if self.root_file.IsZombie():
            raise IOError("'{0}' file does not exist or is an invalid ROOT file")
        # Initialize the attributes
        for name in map(lambda x: x.GetName(),self.root_file.GetListOfKeys()):
            setattr(self,"_{0}".format(name),self.root_file.Get(name))
        # Recreate some useful extra attributes
        total_h = self._total_efficiency.GetTotalHistogram()
        passd_h = self._total_efficiency.GetPassedHistogram()
        self._total_good_tracks= total_h.GetBinContent(1,1)
        self._total_dut_tracks = passd_h.GetBinContent(1,1)
        self._total_efficiency_relevant_bin=self._total_efficiency.FindFixBin(
                passd_h.GetXaxis().GetBinCenter(1),total_h.GetYaxis().GetBinCenter(1))
        # Obtain meta-data
        md_tree = self.root_file.Get("metadata")
        layout_str = ROOT.std.string('')
        md_tree.SetBranchAddress("sensor_layout",layout_str)
        md_tree.GetEntry(0)
        if str(layout_str).find("25x100") != -1 \
                or str(layout_str).find("100x25") != -1:
            self.is_pixel_layout_25by100 = True
        else:
            self.is_pixel_layout_25by100 = False
        # Initialize plot related arguments
        assert 'format' in dir(args['plot_args']),"The 'format' key is needed in the READER "\
                "initialization"
        # convert from the argumentparser Namespace to a dict
        self._plot_args = {}
        for name_arg,value in filter(lambda x: x != 'which',vars(args['plot_args']).iteritems()):
            self._plot_args[name_arg]=value

    def __init__writer(self,outputfile,fe,calibration_active,do_noise_analysis,tdac_mask_file,force_bin=None):
        """Post-processing analysis class encapsulating 
        all the analysis chain.

        Parameters
        ----------
        outputfile: str
            The output filename for the ROOT file
        fe: sensor_description.fe_layout instance
            The senosr layout
        calibration_active: bool
            Whether or not the calibration file is present and therefore the charge
            related histograms (and Moyal's sigma estimation) must be adapted
        do_noise_analysis: bool
            Whether or not perform a noise analysis, using hits not accepted (noisy)
        tdac_mask_file: str|None
            If not None, it should contain the filename where is defined the mask (h5) file            
        ...
        """
        self.__is_reader = False
        self.root_file=ROOT.TFile(outputfile, "recreate")
        self.root_filename = outputfile
        self.cellsize_x=fe.xpitch
        self.cellsize_y=fe.ypitch
        self.is_pixel_layout_25by100 = fe.is_pixel_layout_25by100()
        self.binning_x = {}
        self.binning_y = {}
        self.cell_sizes = [0.5,1,2,4]
        
        # Points used to compare with simulation, they suppose to 
        # characterize different important regions of the pixel. 
        # Defined in a quarter of pixel
        if self.is_pixel_layout_25by100:
            self._adrem_points = dut_analysis.ADREM_POINTS['25x100']
        else:
            self._adrem_points = dut_analysis.ADREM_POINTS['50x50']
        # The radius defining the golden points for the quarter of cell
        self.adrem_radius = 2.5*UM

        # Extra analysis
        self.do_noise_analysis=do_noise_analysis
        # Create the filler if needed
        self.fill_noise_histograms = lambda *argfs: None
        if self.do_noise_analysis:
            self.fill_noise_histograms = self._fill_noise_histograms
        # Idem for the mask analysis
        self.do_mask_analysis = True if tdac_mask_file is not None else False
        self.fill_mask_histograms = lambda *args: None
        if self.do_mask_analysis:
            f = tables.File(tdac_mask_file)
            self.tdac_mask_filename = tdac_mask_file
            # Extract the tdac mask and convert it to ndarray and
            # transpose col->rows, in order to be in the same row/col reference system
            # than in the ntuples
            self.tdac = np.array(f.root['TDAC_mask']).transpose()
            if self.is_pixel_layout_25by100:
                self.tdac = convert_100by25_3D(self.tdac)
            # and close file
            f.close()
            # redefine the histogram filling
            self.fill_mask_histograms = self._fill_mask_histograms
        
        
        # The calibration files are present?
        self.calibration_active=calibration_active
        # if so, some variables are needed to be changed
        if self.calibration_active:
            # FIXME :: double axis? or just electrons?
            self.charge_unit = 'Electrons'
            # This is rough approximation to convert ToT into electrons,
            # but enough for histogram limits, and probably for Moyal sigma 
            # initial estimation
            self.ch_f = 1.1e3
            # Bin increasing factor
            self.bin_f = 3
        else:
            self.charge_unit = "ToT"
            self.ch_f = 1
            self.bin_f = 1

        # The histograms title: to be used as `title_histo("title name","z title")
        # The histos: 
        self._eff_cell_h = {}
        self._clsize_cell_h = {}
        self._tot_cell_h = {}
        self._exptots_cell_h = {}
        self._resx_cell_h = {}
        self._resy_cell_h = {}
        ## And for NOISE
        if self.do_noise_analysis:
            self._noise_clsize_cell_h = {}
            self._noise_tot_cell_h = {}
        ## And for TDAC mask
        #if self.do_mask_analysis:
        #    self._mask_eff_cell_h = {}
        #    self._mask_clsize_cell_h = {}
        #    self._mask_tot_cell_h = {}
        # --- keep track of them
        self._prof2d_cells = {}
        # Cell histograms: so far quarte-pixel (0.5), 1,2 and 4 pixel-cells
        for cell in self.cell_sizes:
            # for 50x50 geometry, same binning in both directions (choose an even number):
            self.binning_x[cell] = self.binning_y[cell] = 160
            if self.is_pixel_layout_25by100:
                # in 25x100 geometry, X is the long side, Y is the short one, to keep square binning we do:
                self.binning_x[cell] *=2 
                self.binning_y[cell] /=2

            # Avoid dots in the name of the histos
            cell_str = str(cell)
            if not isinstance(cell,int):
                cell_str = '{:.1f}'.format(cell).replace('.','dot')
            title_histo = lambda x,y: "{0} vs. x_{4} y_{4};x track mod {2} [mm];"\
                    "y track mod {3} [mm];{1}".format(x,y,cell*self.cellsize_x,cell*self.cellsize_y,"{mod}")
            self._eff_cell_h[cell] = ROOT.TProfile2D("eff_{0}".format(cell_str),\
                    title_histo("DUT efficiency","eff"),\
                    self.binning_x[cell],0.0,float(cell)*self.cellsize_x,self.binning_y[cell],0.0,float(cell)*self.cellsize_y)
            self._clsize_cell_h[cell] = ROOT.TProfile2D("clsize_{0}".format(cell_str),\
                    title_histo("Cluster size","<cluster>"),\
                    self.binning_x[cell],0.0,float(cell)*self.cellsize_x,self.binning_y[cell],0.0,float(cell)*self.cellsize_y)
            self._tot_cell_h[cell] = ROOT.TProfile2D("tot_{0}".format(cell_str),\
                    title_histo("Charge","<Charge_{}> [{}]".format('{clusters}',self.charge_unit)),\
                    self.binning_x[cell],0.0,float(cell)*self.cellsize_x,self.binning_y[cell],0.0,float(cell)*self.cellsize_y)
            self._exptots_cell_h[cell] = ROOT.TProfile2D("exptots_{0}".format(cell_str),\
                    title_histo("Charge: Moyal estimation","<exp(-Charge_{cluster}/#sigma_{global fit})>"),\
                    self.binning_x[cell],0.0,float(cell)*self.cellsize_x,self.binning_y[cell],0.0,float(cell)*self.cellsize_y)
            #self._tot_mpv_cell_h[cell] = ROOT.TProfile2D("tot_mpv_{0}".format(cell),\
            #        title_histo("Charge","<MPV ToT_{cluster}>"),\
            #        self.binning_x[cell],0.0,float(cell)*self.cellsize_x,self.binning_y[cell],0.0,float(cell)*self.cellsize_y)
            self._resx_cell_h[cell] = ROOT.TProfile2D("resx_{0}".format(cell_str),\
                    title_histo("Residuals X-direction","<#Deltax> [mm]"),\
                    self.binning_x[cell],0.0,float(cell)*self.cellsize_x,self.binning_y[cell],0.0,float(cell)*self.cellsize_y)
            self._resy_cell_h[cell] = ROOT.TProfile2D("resy_{0}".format(cell_str),\
                    title_histo("Residuals Y-direction","<#Deltay> [mm]"),\
                    self.binning_x[cell],0.0,float(cell)*self.cellsize_x,self.binning_y[cell],0.0,float(cell)*self.cellsize_y)
            # Get track of them
            self._prof2d_cells[cell] = [ self._eff_cell_h[cell], self._clsize_cell_h[cell], 
                    self._tot_cell_h[cell], self._exptots_cell_h[cell], 
                    self._resx_cell_h[cell], self._resy_cell_h[cell] ]
            # Noise
            if self.do_noise_analysis:
                self._noise_clsize_cell_h[cell] = ROOT.TProfile2D("noise_clsize_{0}".format(cell_str),\
                        title_histo("Cluster size","<cluster>"),\
                        self.binning_x[cell],0.0,float(cell)*self.cellsize_x,self.binning_y[cell],0.0,float(cell)*self.cellsize_y)
                self._noise_tot_cell_h[cell] = ROOT.TProfile2D("noise_tot_{0}".format(cell_str),\
                        title_histo("Charge","<Charge_{}> [{}]".format('{clusters}',self.charge_unit)),\
                        self.binning_x[cell],0.0,float(cell)*self.cellsize_x,self.binning_y[cell],0.0,float(cell)*self.cellsize_y)
                self._prof2d_cells[cell] += [ self._noise_clsize_cell_h[cell], self._noise_tot_cell_h[cell] ]
            ## TDAC Mask
            #if self.do_mask_analysis:
            #    self._mask_eff_cell_h[cell] = ROOT.TProfile2D("mask_eff_{0}".format(cell_str),\
            #            title_histo("DUT efficiency","eff"),\
            #            self.binning_x[cell],0.0,float(cell)*self.cellsize_x,self.binning_y[cell],0.0,float(cell)*self.cellsize_y)
            #    self._mask_clsize_cell_h[cell] = ROOT.TProfile2D("mask_clsize_{0}".format(cell_str),\
            #            title_histo("Cluster size","<cluster>"),\
            #            self.binning_x[cell],0.0,float(cell)*self.cellsize_x,self.binning_y[cell],0.0,float(cell)*self.cellsize_y)
            #    self._mask_tot_cell_h[cell] = ROOT.TProfile2D("mask_tot_{0}".format(cell_str),\
            #            title_histo("Charge","<ToT_{cluster}>"),\
            #            self.binning_x[cell],0.0,float(cell)*self.cellsize_x,self.binning_y[cell],0.0,float(cell)*self.cellsize_y)
            #    self._prof2d_cells[cell] += [ self._mask_eff_cell_h[cell],self._mask_clsize_cell_h[cell], self._mask_tot_cell_h[cell] ]
        
        # Full 2D histograms and profiles
        self._eff_full = ROOT.TProfile2D("eff_full","DUT efficiency vs. x y;col track;row track",\
                (fe.MAX_COL-fe.MIN_COL+1),fe.MIN_COL-0.5,fe.MAX_COL,\
                (fe.MAX_ROW-fe.MIN_ROW+1),fe.MIN_ROW-0.5,fe.MAX_ROW)
        self._res = ROOT.TH2F("res", "residual;#Deltax [mm];#Deltay [mm];Entries", 200, -0.1, 0.1,200,-0.1,0.1)
        self._tot_bcid = ROOT.TH2F("tot_bcid",
                "Cluster charge vs. BCID (seed);Charge_{cluster} ["+self.charge_unit+"];BCID_{seed};Clusters"
                ,200*self.bin_f,0*self.ch_f,200*self.ch_f,32,-0.5,31.5)
        self._clsize_eta_x = ROOT.TH2F("clsize_eta_x",
                "Cluster size vs. #eta on columns;pixels_{cluster}^{col};#eta_{x};Clusters",
                25,-0.5,24.5,100,-0.02,1.02)
        self._clsize_eta_y = ROOT.TH2F("clsize_eta_y",
                "Cluster size on rows;pixels_{cluster}^{row};#eta_{y}",
                25,-0.5,24.5,100,-0.02,1.02)
        self._tot_full = ROOT.TProfile2D("tot_full","Charge [{}] vs. x y;col track;row track".format(self.charge_unit),\
                (fe.MAX_COL-fe.MIN_COL+1),fe.MIN_COL-0.5,fe.MAX_COL,\
                (fe.MAX_ROW-fe.MIN_ROW+1),fe.MIN_ROW-0.5,fe.MAX_ROW)
        # Keep track of the histos
        self._histos2D_full = [ self._eff_full, self._res, self._tot_bcid, 
                self._clsize_eta_x, self._clsize_eta_y, self._tot_full ]
        if self.do_noise_analysis:
            # full 2d histograms and profiles for noise
            self._noise_tot_bcid = ROOT.TH2F("noise_tot_bcid",
                    "Cluster charge vs. BCID (seed);Charge_{cluster} ["+self.charge_unit+"];BCID_{seed};Clusters"
                    ,200*self.bin_f,0*self.ch_f,200*self.ch_f,32,-0.5,31.5)
            self._noise_tot_full = ROOT.TProfile2D("noise_tot_full","Charge [{}] vs. x y;col track;row track".format(self.charge_unit),\
                    (fe.max_col-fe.min_col+1),fe.min_col-0.5,fe.max_col,\
                    (fe.max_row-fe.min_row+1),fe.min_row-0.5,fe.max_row)
            
            # FIXME !!! BUG here, wrong name!! MASTER
            self._histos2d_full += [ self._noise_tot_bcid, self._noise_tot_full ]
        # TDAC mask
        if self.do_mask_analysis:
            # Full 2D histograms and profiles for TDAC MASK
            self._mask_tot_tdac_full = ROOT.TProfile2D("mask_tot_tdac_full",\
                    "TDAC (seed) vs. x y;col track;row track",\
                    (fe.MAX_COL-fe.MIN_COL+1),fe.MIN_COL-0.5,fe.MAX_COL,\
                    (fe.MAX_ROW-fe.MIN_ROW+1),fe.MIN_ROW-0.5,fe.MAX_ROW)
            self._mask_tot_tdac = ROOT.TH2F("mask_tot_tdac",
                    "Cluster charge ["+self.charge_unit+"] vs. TDAC mask value (seed);Charge_{cluster} ]"+self.charge_unit+"];TDAC_{seed};Clusters"
                    ,200*self.bin_f,0*self.ch_f,200*self.ch_f,16,-0.5,15.5)
            
            self._histos2D_full += [ self._mask_tot_tdac_full, self._mask_tot_tdac ]
        
        # Adrem points related histos
        self._adrem_eff = {}
        self._adrem_tot = {}
        for i in range(self._adrem_points.shape[0]):
            xc,yc=self._adrem_points[i]
            self._adrem_eff[i] = ROOT.TProfile2D('adrem_eff_{}_{}'.format(xc/UM,yc/UM).replace('.','dot'),
                    'Quarte-cell point around {},{} (radius {})'.format(xc/UM,yc/UM,self.adrem_radius/UM),
                    10,self._adrem_points[i][0]-self.adrem_radius,self._adrem_points[i][0]+self.adrem_radius,
                    10,self._adrem_points[i][1]-self.adrem_radius,self._adrem_points[i][1]+self.adrem_radius)
            self._adrem_tot[i] = ROOT.TH1F('adrem_tot_{}_{}'.format(xc/UM,yc/UM).replace('.','dot'),
                    'Quarte-cell point around {},{} (radius {})'.format(xc/UM,yc/UM,self.adrem_radius/UM),
                    200*self.bin_f,0*self.ch_f,200*self.ch_f)
        # track them
        self._histos2D_full += self._adrem_eff.values()

        # 1D histograms
        self._clsize = ROOT.TH1F("clsize","Cluster Size;Clusters;pixels_{cluster}",25,-0.5,24.5)
        # Adrem points related histos (see in 2D section)
        # Keep track of the histos
        self._histos1D = [  self._clsize ] + self._adrem_tot.values()
        # 1D histograms for NOISE
        if self.do_noise_analysis:
            self._noise_clsize = ROOT.TH1F("noise_clsize","Cluster Size;Clusters;pixels_{cluster}",25,-0.5,24.5)
            self._histos1D += [ self._noise_clsize ]
        
        # Auxiliary histograms
        self._num = ROOT.TH2F("num",\
                title_histo("DUT","Entries"),
                self.binning_x[cell],0.0,self.cellsize_x,self.binning_y[cell],0.0,self.cellsize_y)
        self._den = ROOT.TH2F("den",\
                title_histo("DUT","Entries"),
                self.binning_x[cell],0.0,self.cellsize_x,self.binning_y[cell],0.0,self.cellsize_y)
        self._observed_efficiency = ROOT.TH1F("observed_efficiency",
                "Observed efficiency per bin",200,0,1.1)
        self._observed_efficiency.Sumw2()
        self._total_efficiency = None
        # Keep track of the histos
        self._histo_auxiliary = [ self._num, self._den, self._observed_efficiency, self._total_efficiency ]

        # Take care of the drift in x and y 
        self._drift_res = np.array([])
        self._drift_res.shape=(0,3)

    def get_adrem_points(self,x,y,is_present):
        """ Search if the given point is within a adrem point (relevant points defined
        in the quarter of cell, used for simulation comparative). Fill related histograms

        Parameters
        ----------
        x: float
            The x-component on the quarter of cell reference system
        y: float
            The y-component on the quarter of cell reference system
        is_present: bool
            Whether or not the estimated track point in the DUT was measured 

        Return
        ------
        (int,(float,float)): the index of the point (see at class-level definition)
            and the current point
        XXX -- NOT USED the point so far, should it be returned only the index?
        """
        p = np.array([x,y])
        for i in range(self._adrem_points.shape[0]):
            if np.linalg.norm(p-self._adrem_points[i]) <= self.adrem_radius:
                self._adrem_eff[i].Fill(x,y,is_present)
                return (i,(x,y))
        return None
    
    def fill_adrem_points(self,i_point,tot):
        """Fill histograms for the adrem point

        Parameters
        ----------
        i_point: (int,(float,float) or None
            The adrem index where the current point belongs to. If the point
            does not belong to any, the value is None. The variable is directly
            provided by the `get_adrem_points` function.
        tot: float|int
            The measured ToT
        """
        if i_point is None:
            return
        self._adrem_tot[i_point[0]].Fill(tot)
         
    def addup_analyses(self,analist,clear=False):
        """In multiprocessing mode, create this instance 
        as adding up the result of the processing of all
        intances in analist

        Parameters
        ----------
        analist: [ dut_analysis instances ]
            The list of all the analysis to be added up
        clear: bool
            Whether or not remove all remnants from the input list 
        """
        # Histograms are added to this histogram
        for hlistname in [ '_histos2D_full', '_histos1D', '_histo_auxiliary' ]:
            hlist=getattr(self,hlistname)
            for h in hlist:
                try:
                    hname = h.GetName()
                except AttributeError:
                    # The histogram doesn't exist yet (probably created at summary time)
                    continue
                for analysis in analist:
                    ph=filter(lambda _hp: _hp.GetName() == hname,filter(lambda _h: _h is not None,getattr(analysis,hlistname)))[0]
                    h.Add(ph)
        # Profiles
        for cell,pflist in self._prof2d_cells.iteritems():
            for pf in pflist:
                pfname = pf.GetName()
                for analysis in analist:
                    ppf=filter(lambda _pfp: _pfp.GetName() == pfname,analysis._prof2d_cells[cell])[0]
                    pf.Add(ppf)
           
        # Other data members with info
        # --- Should be ordered by event (dx,dy,eventnumber)
        drift = np.zeros_like(self._drift_res)
        for analysis in analist:
            drift = np.concatenate((drift, analysis._drift_res ))
        # Order by event (2nd column)
        self._drift_res =  drift[drift[:,2].argsort()]

        # Remove the ROOT files created by the analysis
        for analysis in analist:
            os.remove(analysis.root_filename)

    def summarize(self):
        """Evaluates the total efficiency, creates a TEfficiency
        object which is stored in the output file, as well as other
        attributes useful to dump out useful information
        """
        if self.__is_reader:
            return
        # Create the observed efficiency by just using one bin
        den1bin=self._den.Clone("denprov")
        num1bin=self._num.Clone("numprov")
        for h in [den1bin,num1bin]:
            dum = h.Rebin2D(h.GetNbinsX(),h.GetNbinsY())
            h.SetDirectory(0)
        self._total_efficiency = ROOT.TEfficiency(num1bin,den1bin)
        self._total_efficiency.SetName("total_efficiency")
        self._total_efficiency.SetDirectory(self.root_file)
        # XXX: Uses a different prior whenever we are in perp. incidence, 
        #      there is regions with low efficiencies already known
        self._total_efficiency.SetStatisticOption(ROOT.TEfficiency.kBUniform)
        self._total_efficiency_relevant_bin=self._total_efficiency.FindFixBin(
                den1bin.GetXaxis().GetBinCenter(1),den1bin.GetYaxis().GetBinCenter(1))
        # Fill the observed efficiency histo from the per 1-cell efficiency map
        histo_oeff = self._eff_cell_h[1].Clone('prov_obs_eff')
        histo_oeff.SetDirectory(0)
        bins_x = self.binning_x[1]
        bins_y = self.binning_y[1]
        # But first, it must be sure the statistic per bin is enough
        while histo_oeff.GetEntries()/(bins_x*bins_y) < self.min_bin_content:
            bins_x = int(0.5*bins_x)
            bins_y = int(0.5*bins_y)
            histo_oeff.Rebin2D(2,2)
        for xbin in xrange(1,histo_oeff.GetNbinsX()+1):
            for ybin in xrange(1,histo_oeff.GetNbinsY()+1):
                self._observed_efficiency.Fill(
                        histo_oeff.GetBinContent(xbin,ybin),
                        histo_oeff.GetBinError(xbin,ybin)
                        )
        # FIXME --- TODO
        # Fit observed efficiency to extract the errors, assuming a skew
        # Skew normal distribution: https://en.wikipedia.org/wiki/Skew_normal_distribution
        #gaus = ROOT.TF1('g','2/[2]*gaus(x)*0.5*(1.+TMath::Erf([3]*((x-[1])/(sqrt(2)*[2]))))',xmin,xmax)
        # FIXME --- TODO
        # Fill some useful datamembers
        self._total_good_tracks= den1bin.GetBinContent(1,1)
        self._total_dut_tracks = num1bin.GetBinContent(1,1)

        # Finally auxiliary stuff
        # XXX - Si no, rellena els huecos... xrange(self._drift_res.[-1][2])
        # XXX   for evt in all evetnt: if no evnt no present, ignore, otherwise fill graph      
        valid_evts = array.array('f',self._drift_res[:,2])
        self._drift_gx = ROOT.TGraph(self._drift_res.shape[0],valid_evts,array.array('f',self._drift_res[:,0]))
        self._drift_gx.SetName('drift_x_evt')
        self._drift_gy = ROOT.TGraph(self._drift_res.shape[0],valid_evts,array.array('f',self._drift_res[:,1]))
        self._drift_gy.SetName('drift_y_evt')
        self._grphs = [ self._drift_gx, self._drift_gy ]
        
        # Adjust a polinomial function and if there is a drift warn
        # Allowed maximum variation: 1um --> 1e-3 mm (after the total events)
        # (8um is bit more than the telescope resolution)
        max_var = 0.5e-3
        for plane,gr in [('X',self._drift_gx),('Y',self._drift_gy)]:
            gr.Fit('pol1','Q')
            try:
                ff=gr.GetListOfFunctions()[0]
            except IndexError:
                print 
                print("\033[1;31mAlignment drift check PROBLEM:\033[1;m"\
                        "Polinomial fit failed for the graph: '{0}'. Ignoring the check".format(plane))
                continue
            # Get the index
            last_index = gr.GetN()-1
            # get the points
            last_point = gr.GetX()[last_index]
            total_drift_mean = abs(ff(0)-ff(last_point))
            
            if total_drift_mean > max_var:
                drift_per_evt,errd = ff.GetParameter(1),ff.GetParError(1)
                x0,x0_errd = ff.GetParameter(0),ff.GetParError(0)
                print 
                print("\033[1;33mAlignment drift check WARNING:\033[1;m "\
                        "Detected a drift in the PLANE '{0}' larger than 3um "\
                        "[Total Drift:{1:.1f} um]".format(plane,total_drift_mean*1e3))
                print("\033[1;33mAlignment drift check WARNING:\033[1;m "\
                        "Linear correction to predicted '{0}': {1}*event_number+{2}".format(plane,drift_per_evt,x0))
                # XXX -- Provide the errors --> Should be taken into account ... 

    def plot_histo(self,histo,preliminary=True):
        """Plot the histogram in the given format using the 
        `.extra_function.plot_histo` function

        Parameters
        ----------
        histo: ROOT.TH --> XXX FIXME TO BE CHANGE to str
            The histogram to be plotted
        preliminary: bool
            Whether or not to include the word preliminary after CMS
            XXX -- Better to create a flag to include or not CMS -- XXX
        
        Returns
        -------
        plotname: str   
            The created plotname 
        """
        #raise NotImplementedError,"Needs to be adapted yet!!"
        #from sifca_utils import plotting as pl
        return plot_histo(histo,
                self.is_pixel_layout_25by100,
                self._plot_args,
                self.root_file.GetName().replace(".root",""),
                preliminary)
    
    def plot_all(self):
        """
        """
        for h in filter(lambda xx: isinstance(xx,ROOT.TH1),map(lambda x: getattr(self,x), dir(self))):
            self.plot_histo(h,self._plot_args['format'])

    def print_summary(self):
        assert hasattr(self,"_total_dut_tracks"),"Cannot call 'print_summary'"\
                " function before calling 'summarize' first"
        b=self._total_efficiency_relevant_bin
        print 
        print ">>> Hit efficiency evaluation"        
        print ">>> Total Global  Efficiency: {0:.4f}+{1:.4f}-{2:.4f}".\
                format(self._total_efficiency.GetEfficiency(b),
                        self._total_efficiency.GetEfficiencyErrorUp(b),\
                        self._total_efficiency.GetEfficiencyErrorLow(b))
        print ">>> Most Probable Efficiency: {0:.4f}+TODO".\
                format(self._observed_efficiency.GetBinCenter(self._observed_efficiency.GetMaximumBin()))
                        #TODO: Extract around the found value, the values which
                        #      covers 68% (1sigma) or  95% (2sigmas)
        print ">>> Number of GOOD tracks: {0:.0f}".format(self._total_good_tracks) 
        print ">>> Number of DUT  tracks: {0:.0f}".format(self._total_dut_tracks) 

    def fill_histograms(self,pred_point,dut_id,meas_dut_hit,mes):
        """Fill all efficiency histograms

        XXX --- Accept x/y cell and col/row pred in order to avoid
        recalculation

        Parameters
        ----------
        pred_points: (float,float)
            The track predicted points in the DUT plane in local coordinates
        dut_it: int
            The DUT plane ID
        meas_dut_hit: bool
            Whether or not a meas
        mes: ROOT.TTree
            The measured hit tree

        Return
        ------
        hit_id: int
            The index of the measured hit id used, or -1 if any
        """
        if self.__is_reader:
            return
        # We are here, then good track
        # Obtain the point in the cells
        x_cel = {}
        y_cel = {}
        col_pred = {}
        row_pred = {}
        # reverse order to assure cell-1 is filled before cell 0.5
        for cell in sorted(self._prof2d_cells.keys(),reverse=True):
            if cell < 1:
                # Extra reflection to the 1-cell
                x_cel[cell] = x_cel[1]
                if x_cel[1] > 0.5*self.cellsize_x:
                    x_cel[cell] = self.cellsize_x-x_cel[1]
                y_cel[cell] = y_cel[1]
                if y_cel[1] > 0.5*self.cellsize_y:
                    y_cel[cell] = self.cellsize_y-y_cel[1]
                # Points to be used for TDAC simulation comparation 
                adrem_point = self.get_adrem_points(x_cel[cell],y_cel[cell],meas_dut_hit)
            else:
                x_cel[cell] = pred_point[0]-int(pred_point[0]/(float(cell)*self.cellsize_x))*float(cell)*self.cellsize_x
                y_cel[cell] = pred_point[1]-int(pred_point[1]/(float(cell)*self.cellsize_y))*float(cell)*self.cellsize_y
            # Fill efficiency in the cell
            self._eff_cell_h[cell].Fill(x_cel[cell],y_cel[cell],meas_dut_hit)
        # -- For the efficiency calculation, take just the cell-1
        # Fill histograms: efficiency related
        self._den.Fill(x_cel[1],y_cel[1])
        # See init: pitch = cellsize
        col_pred = pred_point[0]/self.cellsize_x
        row_pred = pred_point[1]/self.cellsize_y
        # -- all sensor efficiency
        self._eff_full.Fill(col_pred,row_pred,meas_dut_hit)
        # Hit measured matched
        if meas_dut_hit:
            # for eff. calculation (take values at cell-1)
            self._num.Fill(x_cel[1],y_cel[1])
            # Find closest measured hit to the track predicted point
            meas_ihit=closest_measured_hit(dut_id,pred_point,mes)
            # Fill histograms with measured hit instead of track predicted point
            dx = mes.xPos[meas_ihit]-pred_point[0]
            dy = mes.yPos[meas_ihit]-pred_point[1]
            # -- To evaluate a potential drift on the positions
            self._drift_res = np.append(self._drift_res,[(dx,dy,mes._store.GetReadEntry())],axis=0)
            self._res.Fill(dx,dy)
            # 1-dimesional cluster obs.
            self._tot_bcid.Fill(mes.tot[meas_ihit],mes.bcid[meas_ihit])
            self._clsize.Fill(mes.clusterSize[meas_ihit])
            self._clsize_eta_x.Fill(mes.clusterSize_x[meas_ihit],mes.eta_x[meas_ihit])
            self._clsize_eta_y.Fill(mes.clusterSize_y[meas_ihit],mes.eta_y[meas_ihit])
            # -- charge over the sensor
            self._tot_full.Fill(col_pred,row_pred,mes.tot[meas_ihit])
            # fill cell-related histos
            for cell in self._prof2d_cells.keys():
                self._clsize_cell_h[cell].Fill(x_cel[cell],y_cel[cell],mes.clusterSize[meas_ihit])
                self._tot_cell_h[cell].Fill(x_cel[cell],y_cel[cell],mes.tot[meas_ihit])
                self._exptots_cell_h[cell].Fill(x_cel[cell],y_cel[cell],np.exp(-mes.tot[meas_ihit]/self.sigma_tot))
                self._resx_cell_h[cell].Fill(x_cel[cell],y_cel[cell],dx)
                self._resy_cell_h[cell].Fill(x_cel[cell],y_cel[cell],dy)
            # The golden points for simulation comparation
            self.fill_adrem_points(adrem_point,mes.tot[meas_ihit])

            return meas_ihit
        return -1

    def _fill_noise_histograms(self,mes,dut_id,used_hits):
        """Fill all noise related histograms, i.e. quantities
        related with hits not matched with a track

        XXX - Define extra criteria to decide if a hit is noise, for
              instance, being outside the time window (BCID)

        Parameters
        ----------
        mes: ROOT.TTree
            The measured hit tree
        dut_id: int
            The DUT plane ID
        used_hits: list(in)
            The list of hits associated to tracks in this event
        """
        if self.__is_reader:
            return
        for ihit in filter(lambda di: mes.sensorId[di] == dut_id, \
                filter(lambda dii: dii not in used_hits,range(len(mes.xPos)))):
            # See init: pitch = cellsize
            col = mes.xPos[ihit]/self.cellsize_x
            row = mes.yPos[ihit]/self.cellsize_y
            col_cel = {}
            row_cel = {}
            for cell in self._prof2d_cells.keys():
                col_cel[cell] = mes.xPos[ihit]-int(mes.xPos[ihit]/(float(cell)*self.cellsize_x))*float(cell)*self.cellsize_x
                row_cel[cell] = mes.yPos[ihit]-int(mes.yPos[ihit]/(float(cell)*self.cellsize_y))*float(cell)*self.cellsize_y
            # 1-dimesional cluster obs.
            self._noise_tot_bcid.Fill(mes.tot[ihit],mes.bcid[ihit])
            self._noise_clsize.Fill(mes.clusterSize[ihit])
            # -- charge over the sensor
            self._noise_tot_full.Fill(col,row,mes.tot[ihit])
            # fill cell-related histos
            for cell in self._prof2d_cells.keys():
                self._noise_clsize_cell_h[cell].Fill(col_cel[cell],row_cel[cell],mes.clusterSize[ihit])
                self._noise_tot_cell_h[cell].Fill(col_cel[cell],row_cel[cell],mes.tot[ihit])
    
    def _fill_mask_histograms(self,pred_point,mes,meas_ihit):
        """Fill all TDAC mask analysis related histograms, i.e. taking into 
        account the TDAC mask provided by the user (and stored at the metadata)


        Parameters
        ----------
        pred_points: (float,float)
            The track predicted points in the DUT plane in local coordinates
        mes: ROOT.TTree
            The measured hit tree
        meas_ihit: int
            The hit index
        """
        if self.__is_reader:
            return
        # 1-dimesional cluster obs, but just filling with cluster size-1
        if meas_ihit>=0 and mes.clusterSize[meas_ihit] == 1:
            # Get the col and row closest to the maximum ToT (seed)
            col_pred = int(np.round(pred_point[0]/self.cellsize_x))
            row_pred = int(np.round(pred_point[1]/self.cellsize_y))
            self._mask_tot_tdac_full.Fill(col_pred,row_pred,self.tdac[row_pred][col_pred])
            self._mask_tot_tdac.Fill(mes.tot[meas_ihit],self.tdac[row_pred][col_pred])

    def write(self,ref_plane=None,dut_plane_id=None):
        """Write out to disk

        Parameters:
        ----------
        ref_plane: int
            The reference plane z-ordered position, it can be used
            to feed the `--ref|-r` option [Optional]
        dut_plane_id: (int,int) 
            The plane ID and the z-ordered position, it can be used
            to feed the `--dut|-d` option [Optional]
        """
        if self.__is_reader:
            return
        # Just be sure enough statistic is present for the cell plots
        # Minimum of 70 entries/cell in average
        for cell,hlist in self._prof2d_cells.iteritems():
            hproxy=hlist[0]
            while hproxy.GetEntries()/(self.binning_x[cell]*self.binning_y[cell]) < self.min_bin_content:
                self.binning_x[cell] = int(0.5*self.binning_x[cell])
                self.binning_y[cell] = int(0.5*self.binning_y[cell])
                for h in hlist:
                    h.Rebin2D(2,2)
                if cell == 1:
                    self._den.Rebin2D(2,2)
                    self._num.Rebin2D(2,2)

        # After re-bin, re-create the mpv_tot plot with the final 
        # values -sigma ln(<exp(x/sigma)>) 
        self._mpv_tot = ROOT.TH1F('tot_mpv',
                '<MPV> per bin at cell;<MPV> [{}];#Cells in the 2D histos'.format(self.charge_unit),
                100*self.bin_f,0*self.ch_f,49*self.ch_f)
        self._histos1D.append( self._mpv_tot )
        self._tot_mpv_cell_h = {}
        for cell,_hh in self._exptots_cell_h.items():
            # New histos
            cell_str = str(cell)
            if not isinstance(cell,int):
                cell_str = '{:.1f}'.format(cell).replace('.','dot')
            self._tot_mpv_cell_h[cell] = ROOT.TProfile2D('tot_mpv_{0}'.format(cell_str),
                    "Cluster vs. x_{2} y_{2};x track mod {0} [mm];"\
                            "y track mod {1} [mm];{3}".format(cell*self.cellsize_x,cell*self.cellsize_y,"{mod}",
                                "<MPV_{cluster}> ["+self.charge_unit+"]"),
                    _hh.GetNbinsX(),_hh.GetXaxis().GetBinLowEdge(1),_hh.GetXaxis().GetBinUpEdge(_hh.GetNbinsX()),
                    _hh.GetNbinsY(),_hh.GetYaxis().GetBinLowEdge(1),_hh.GetYaxis().GetBinUpEdge(_hh.GetNbinsY()))
            for i in range(1,_hh.GetNbinsX()+1):
                for j in range(1,_hh.GetNbinsY()+1):
                    q = -self.sigma_tot*np.log(_hh.GetBinContent(i,j))
                    x = _hh.GetXaxis().GetBinCenter(i)
                    y = _hh.GetYaxis().GetBinCenter(j)
                    self._tot_mpv_cell_h[cell].Fill(x,y,q,_hh.GetBinEntries(_hh.GetBin(i,j)))
                    self._mpv_tot.Fill(q)

        # Some metadata
        md_tree = ROOT.TTree("metadata","Metadata")
        # layout
        if self.is_pixel_layout_25by100:
            layout=ROOT.std.string("25x100")
        else:
            layout=ROOT.std.string("50x50")
        md_tree.Branch("sensor_layout",layout)
        # drift corrected
        if self._dx_drift_applied is not None:
            dx_corr=ROOT.std.string("x(event)={0}+(event)*({1})".format(*self._dx_drift_applied))
            md_tree.Branch("correction_drift_x_applied",dx_corr)
        if self._dy_drift_applied is not None:
            dy_corr=ROOT.std.string("y(event)={0}+(event)*({1})".format(*self._dy_drift_applied))
            md_tree.Branch("correction_drift_y_applied",dy_corr)
        # Reference and DUT position if any
        if ref_plane is not None:
            # XXX a int?
            rp_str = ROOT.std.string("z_position={0}".format(ref_plane))
            md_tree.Branch("ref_plane",rp_str)
        if dut_plane_id is not None:
            # XXX check the format
            # XXX a couple of ints?
            dp_str= ROOT.std.string("z_position={0},ID={1}".format(dut_plane_id[0],dut_plane_id[1]))
            md_tree.Branch("dut_plane",dp_str)
        # The estimated global sigma from the Moyal distribution fit to 
        # the ToT 1dim distribution
        sigma_moyal = ROOT.std.string("{0:.4f}".format(self.sigma_tot))
        md_tree.Branch("global_sigma_moyal",sigma_moyal)
        # Blahbblah
        # Global efficiency
        eff = ROOT.std.string("{0:.4f} +{1:.4f}-{2:.4f}".format(\
                self._total_efficiency.GetEfficiency(self._total_efficiency_relevant_bin),
                self._total_efficiency.GetEfficiencyErrorUp(self._total_efficiency_relevant_bin),
                self._total_efficiency.GetEfficiencyErrorLow(self._total_efficiency_relevant_bin)))
        md_tree.Branch("global_efficiency",eff)
        # The TDAC analysis, keep track of the mask file
        if self.do_mask_analysis:
            # XXX check the format
            # XXX a couple of ints?
            maskfilename= ROOT.std.string(os.path.basename(self.tdac_mask_filename))
            md_tree.Branch("tdac_mask_file",maskfilename)
        # filling the metadata
        md_tree.Fill()
        # And graphs
        for gr in self._grphs:
            gr.Write()
        # Storing all 
        self.root_file.Write()
        self.root_file.Close()

    def pre_calculate_sigma_moyal(self,tree,dutid):
        """XXX DOC
        Parameters
        ----------
        tree: ROOT.TTree
            The meashits tree from EUTelbalbhla... XXX
        """
        # Create the total distribution
        # FIXME -- Hardcoded  bin and ranges?
        h = ROOT.TH1F('prov_tot_global','',50*self.bin_f,0*self.ch_f,49*self.ch_f)
        # Rough estimation a ToT -> ch_f
        filled_n = tree.Project(h.GetName(),'tot*{}'.format(self.ch_f),'sensorId=={}'.format(dutid))
        if filled_n == 0:
            raise RuntimeError('Trying to estimate sigma from Charge distribution:"\
                    " Not available entries for "tot" branch and "sensorId=={}"'.format(dutid))
        # Estimate the sigma
        # XXX _ any print ? 
        #print()
        moyal = automatic_moyal_fit(h)
        self.sigma_tot = moyal.GetParameter('sigma')
        print ">>> \033[1;32mToT estimation\033[1;m:"\
                " Moyal distribution fit, sigma={0:.2f}".format(self.sigma_tot)
        # any print ?

