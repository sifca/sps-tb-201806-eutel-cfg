#!/usr/bin/env python
"""Cluster processing algorithms

XXX DOC
XXX Check of invariants

jorge.duarte.campderros@cern.ch CERN/IFCA 2020.03.30
"""
__author__ = "Jordi Duarte-Campderros"
__credits__ = ["Jordi Duarte-Campderros"]
__version__ = "0.1"
__maintainer__ = "Jordi Duarte-Campderros"
__email__ = "jorge.duarte.campderros@cern.ch"
__status__ = "Development"

import functools

import numpy as np
from scipy.ndimage import measurements as cluster_finder
from scipy.spatial import distance

import ROOT

from sensor_description import sensor_layout,MM

MATCH_RADIUS = 0.6*MM
        
class Clusters(object):
    """Cluster finding and cluster estimation algorithms. The class mimics
    the methods of `ntuple_utils.hits` class (the measurement TTree of the 
    ntuple) in order to be used transparently for any client.
    A part from the same methods (branches in the original TTree) then 
    `ntuple_utils.hits`, the class contain the specific cluster algorithms.
    """
    # All branches available on the meashits TTree
    _mimicked_branches = [ '_nHits', '_xPos', '_yPos', '_eta_x', '_eta_y',
            '_tot', '_sensorId', '_bcid', 
            '_clusterSize', '_clusterSize_x', '_clusterSize_y' ]
    
    # Decorator to check if the clusters present in the branches
    # are the current event
    def update_clusters(func):
        @functools.wraps(func)
        def check_process_wrapper(self,*args,**kwargs):
            if self._current_event >= 0 and self._current_event != self._event_processed[-1]:
                # Launch clusterization algorithms, and actualize counter
                #print("Clustering...")
                self._clusterize()
                self._event_processed.append(self._current_event)
            return func(self,*args,**kwargs)
        return check_process_wrapper
    
    def __init__(self):
        self._current_event = -1
        self._event_processed = [-1]

        # To control the only 1 dut mode 
        self._use_only_dut = None

        # Point to a dummy function until the pixel tree is not attached
        self.GetEntry = self._aux_trees_not_present
        self.GetReadEntry = self._aux_trees_not_present

        # Cluster position estimator algorithm (currently only 
        # present center of gravity)
        self.particle_position = self._cog_estimator
        
        # Initialize all branches
        for mbr in Clusters._mimicked_branches:
            setattr(self,mbr,None)
        self._modified_hitPattern = None

    # All branches available on the meashits TTree
    @property
    @update_clusters
    def nHits(self):
        return self._nHits

    @property
    @update_clusters
    def xPos(self):
        return self._xPos

    @property
    @update_clusters
    def yPos(self):
        return self._yPos
    
    @property
    @update_clusters
    def eta_x(self): 
        return self._eta_x

    @property
    @update_clusters
    def eta_y(self):
        return self._eta_y

    @property
    @update_clusters
    def tot(self):
        return self._tot

    @property
    @update_clusters
    def sensorId(self):
        return self._sensorId
    
    @property
    @update_clusters
    def bcid(self):
        return self._bcid

    @property
    @update_clusters
    def clusterSize(self):
        return self._clusterSize

    @property 
    @update_clusters
    def clusterSize_x(self):
        return self._clusterSize_x

    @property
    @update_clusters
    def clusterSize_y(self):
        return self._clusterSize_y

    @property
    @update_clusters
    def modified_hitPattern(self):
        # need some link to the track?
        return self._modified_hitPattern

    def _aux_trees_not_present(self,event):
        raise RuntimeError("Trying to use this class without previously"\
                " attached the `rawdata` and `fitpoints` TBTree ")

    def attach_trees(self,rawdata_tree,fitpoints_tree,tracks_tree):
        """Attach the pixel and fitpoints TBTree and modify the hitPattern property
        of the `TBTrack.tracks` tree
        """
        self._pixels_tree = rawdata_tree
        self._fitpoints_tree = fitpoints_tree
        # This cluster instance take control of the calculation of the 
        # hitPattern
        tracks_tree.delegate_hitPattern(self)

        # Now is ready
        self.GetEntry = self._get_entry
        self.GetReadEntry = self._get_current_entry

    def _get_current_entry(self):
        """Returns the current entry/event number

        Returns
        -------
        entry number: int
        """
        return self._current_event

    def _get_entry(self,i):
        """Mimic the GetEntry method from the TTree, given that this
        class has been used instead of the real TTree. Therefore, calling
        theis method will process the cluster finding using all the available 
        pixels on the current event. The method must be called after the pixel
        GetEntry method.

        Parameters
        ----------
        i: int
            The event number
        """
        self._current_event = i
        # Initialize all branches (or delete?)
        for mbr in Clusters._mimicked_branches:
            setattr(self,mbr,None)
    
    def get_connected(self,pxid_list):
        """Pattern recognition algorithm: associates from the list of elements
        those which are connected. See `scipy.cluster_finder` algorithm for
        details

        Parameters
        ----------
        pxid_list: np.array(int)
            The list of pixel indices to search for

        Returns
        -------
        list(np.array(int)): the list of clusters found. Each cluster is 
            defined by the list of pixel indices
        """
        # get columns and rows
        pos = self._pixels_tree.get_positions(pxid_list)
        # Create the image input to the clustering finding function
        im=np.zeros(shape=(np.max(pos[:,0])+1,np.max(pos[:,1])+1))
        # with pixels actives in the columns and rows 
        im[pos[:,0],pos[:,1]]=1
        # allowing connections with up, down and diagonal directions
        lw,num=cluster_finder.label(im,structure=[[1,1,1],[1,1,1],[1,1,1]])
        # creates the different set for each cluster found
        clusters_list = []
        for iset in xrange(num):
            # obtain the index of the local pxid_list
            local_id=np.argwhere(lw[pos[:,0],pos[:,1]] == iset+1).flatten()
            # extract the actual pixel index and append the list
            clusters_list.append(np.array(pxid_list)[local_id])
        return clusters_list

    def _cog_estimator(self,pxid_list):
        """Center of Gravity algorithm
        
        Parameters
        ----------
        pxid_list: np.array(int)
            The list of pixel indices to search for
        """
        # Weight for the charges
        weights = np.array(map(lambda idpx: self._pixels_tree.get_charge(idpx),pxid_list))
        # Positions 
        pos = self._pixels_tree.get_positions(pxid_list)
        # Get the average weighted
        return np.average(pos,weights=weights,axis=0)

    def _local_coordinates(self,dut,colrow):
        """Return the local coordinate in [mm] given the
        local coordinates in pixel units (colr,rows)

        Parameters
        ---------
        dut: int
            The DUT identifier
        colrow: np.array( [cols,rows]]), shape = (n,2)
            The pixels in column and rows

        Returns
        -------
        xypos: np.array([x,y]), shape=(,2)
        """
        # Place the point in the middle of the pitch, i.e. the center 
        # of the pixel
        x = (colrow[:,0]+0.5)*self.xpitch[dut]
        y = (colrow[:,1]+0.5)*self.ypitch[dut]
        return np.array(zip(x,y))

    def _clusterize(self):
        """Wokhorse class, cluster finding and cluster position estimator 
        algorithms are launched here using the current event data.
        """
        # Initialization on the current event
        self._sensorId = np.array([],dtype=np.int8)
        self._xPos = np.array([])
        self._yPos = np.array([])
        self._tot  = np.array([])
        #self._bcid = np.array([])
        self._clusterSize = np.array([],dtype=np.int8)
        self._clusterSize_x = np.array([],dtype=np.int8)
        self._clusterSize_y = np.array([],dtype=np.int8)

        clusters = np.array([])
        cpe      = np.array([])
        cpelocal = np.array([])
        nclusters= 0
        # Extract info for each DUT present
            
        # XXX ??
        used_predicted = [] 
        # XXX ??

        # Prepare the hit pattern to be modified depending whether 
        # a reconstructed cluster is matched or not to a track
        self._modified_hitPattern = np.array(self._original_hitPattern.data())
        for dut,indices in self._pixels_tree.get_indices_per_dut(self._use_only_dut).iteritems():
            if len(indices) == 0:
                continue
            # Check if there is fitpoints in the event for this dut
            mask_fit = np.where(np.array(self._fitpoints_tree.sensorId.data()) == dut)[0]
            is_fitpoints_empty = len(mask_fit) == 0
            # =====================================
            # Cluster finding
            clusters = self.get_connected(indices)
            # =====================================
            # Cluster position estimation
            cpe = np.zeros(shape=(len(clusters),2))
            charge = np.zeros(len(clusters))
            for i,cl in enumerate(clusters):
                # =====================================
                # Cluster Position Estimation
                cpe[i] = self.particle_position(cl)
                # Total charge of the cluster
                charge[i] = self._pixels_tree.get_total_charge(cl)
                # Sensor
                self._sensorId = np.concatenate((self._sensorId,np.array([dut])))
                # BCID (taking the one from the seed?) So far, just taking the first-pixel
                #self._bcid = np.concatenate((self._bcid,np.array([self._.bcid[cl[0]]])))
                # Number of pixels
                self._clusterSize = np.concatenate((self._clusterSize,np.array([len(cl)])))
                self._clusterSize_x = np.concatenate((self._clusterSize_x,np.array([len(set(cpe[:,0]))])))
                self._clusterSize_y = np.concatenate((self._clusterSize_y,np.array([len(set(cpe[:,1]))])))
            cpelocal = self._local_coordinates(dut,cpe)
            self._xPos = np.concatenate((self._xPos,cpelocal[:,0]))
            self._yPos = np.concatenate((self._yPos,cpelocal[:,1]))
            self._tot = np.concatenate((self._tot,charge))
            # Measured points 
            nclusters += len(clusters)

            # =====================================
            # Matching predicted points and tracks
            # hit pattern modification
            if len(clusters) > 0 and not is_fitpoints_empty:
                # Get the predicted points in the dut
                predpoints = np.array(zip(np.array(self._fitpoints_tree.xPos.data())[mask_fit],
                    np.array(self._fitpoints_tree.yPos.data())[mask_fit]))
                # Build distance matrix between measurements and predpoints. Each element
                # of the matrix d_{ij} contains the distance between the i-measurement and 
                # the j-predictions. 
                dists = distance.cdist(cpelocal,predpoints,'euclidean')
                # Matching condition
                matched = dists < MATCH_RADIUS
                # Order by distance with respoct the measurements (axis=1)
                jsorted = np.argsort(dists)
                
                # Get track of what prediction matched with the measurement
                # i-(key) dut wise index - measurement
                # j-(value) absolute index - fitpoints/tracks
                i_matched = {}
                # Prepare the assignation algorithm: do not repeat assigned predictions
                j_used = []
                for imeas in range(len(cpelocal)):
                    # Dealing with the jsorted indixation
                    jsort_list = filter(lambda jj: jj not in j_used,jsorted[imeas,:][matched[imeas,:]])
                    if len(jsort_list) == 0:
                        continue
                    # Be sure do not used again the assigned point
                    j_used.append(jsort_list[0])
                    # And remember to convert the predicted index into the predpoints-array index
                    jpred =  jsorted[imeas,jsort_list[0]]
                    # And to the absolute indexing (as in the original TTree)
                    i_matched[imeas] = mask_fit[jpred]
                    # Modify hit pattern on the track
                    self._modified_hitPattern[jpred] |= (1 << self.dutplane[dut])

            # Update those hitPatterns of tracks that weren't matched with no measurement
            # XXX -- DON'T DO THAT!! ... YET, it has to be cross-checked.
            ##for k in filter(lambda kk: kk not in list(set(i_matched.values())),mask_fit):
            ###    if (self._modified_hitPattern[k] != self._original_hitPattern[k]):
            ###    # Clear the bit 
            ###    self._modified_hitPattern[k] &= ~(1 << self.dutplane[dut])

        # Ready to fill remaining branches
        self._nHits = np.array([nclusters],dtype=np.int8)
        # --- Not yet implemented
        self._eta_x = np.zeros_like(self._xPos) 
        self._eta_y = np.zeros_like(self._xPos) 
        self._bcid = np.zeros_like(self._xPos) 
    
