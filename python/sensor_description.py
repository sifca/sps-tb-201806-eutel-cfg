#!/usr/bin/env python
"""Sensor description classes

* sensor_layout
* fe_layout
* hit_mask

jorge.duarte.campderros@cern.ch CERN/IFCA 2018.11.13
"""
__author__ = "Jordi Duarte-Campderros"
__credits__ = ["Jordi Duarte-Campderros"]
__version__ = "0.1"
__maintainer__ = "Jordi Duarte-Campderros"
__email__ = "jorge.duarte.campderros@cern.ch"
__status__ = "Development"

import ROOT
import numpy as np

MM=1.0

class sensor_layout(object):
    """Defines the geometrical sensor layout, i.e. 
    the pixel size and pitch 
    """
    # Enumerate for sensor geometries
    L25x100=0
    L50x50 =1
    # Fixed sensor size
    XSIZE = 20.0*MM
    YSIZE=9.6*MM
    def __init__(self,layout):
        """Defines the geometrical layout of the sensor. It could
        be different than the ROC, which is always 50x50, in that 
        case a mapping between the ROC and the sensor is needed.
    
        Parameters
        ----------
        layout: str
            The sensor pixels layout, valid values are "25x100" and "50x50"
        """
        if layout == "25x100" or layout == "100x25":
            self._layout = sensor_layout.L25x100
            self._layout_str="25x100"
            self.xpitch = 0.1*MM
            self.ypitch = 0.025*MM
        elif "50x50":
            self._layout = sensor_layout.L50x50
            self._layout_str="50x50"
            self.xpitch = self.ypitch = 0.050*MM
        else:
            raise RuntimeError("Invalid sensor pixel layout '{0}'".format(layout))
        self._min_col = 0
        self._min_row = 0
        # Remove the last one (remember firts column/row at 0)
        self._max_col = int(round(sensor_layout.XSIZE/self.xpitch-1,1))
        self._max_row = int(round(sensor_layout.YSIZE/self.ypitch-1,1))

    def columns(self):
        """Return the minimum and maximum valid columns

        Return
        ------
        (int,int)
        """
        return (self._min_col,self._max_col)

    def rows(self):
        """Return the minimum and maximum valid rows

        Return
        ------
        (int,int)
        """
        return (self._min_row,self._max_row)

    def layout(self):
        """The geometrical layout

        TO BE DEPRECATED

        Return: int
            L25x100 or L50x50 enumerates
        """
        return self._layout

    def is25by100(self):
        return self._layout == sensor_layout.L25x100

    def is50by50(self):
        return self._layout == sensor_layout.L50x50

class fe_layout(object):
    """Define the different Front Ends available 
    at a RD53A chip, and which are activated
    """
    # Remove Edge columns/rows
    RE=3
    
    # Note that this is the RD53A layout, it is always 50x50
    MIN_COL=0
    MAX_COL=399
    
    MIN_ROW=0
    MAX_ROW=191
    
    COL_MAP = { 'sync': (MIN_COL+RE,127-RE), 'lin': (128+RE,263-RE),\
            'diff': (264+RE,MAX_COL-RE) }
    # XXX  col and row limits to be defined as properties?
    # XXX  encapsulating then the re-mapping behaviour
    def __init__(self,sensor_geometry,**kwd):
        """The active layout of the ROC

        Parameters
        ----------
        sensor_geometry: sensor_layout instance
            The geometrical layout of the sensor. If is 25x100, a mapping
            is needed between the ROC and the sensor pixels
        kwd arguments: 
            Activate the analog front ends. Per default are deactivated, 
            an exception will raise if no FE is on
             lin_active: bool 
             sync_active: bool
             diff_active: bool 

        Raises
        ------
        RuntimeError:
            Whenever no FE has been activated explicitely, or try to 
            activate non-consecutive regions
        ValueError:
            Whenever a FE doesn't not exist

        Example
        -------
        An example code to use a 25x100 sensor activating linear and sync:
        
        >>> sl = sensor_layout("25x100")
        >>> roc= fe_layout(sl,lin_active=True,sync_active=True)
        ...
        """ 
        # Initial values
        min_col = fe_layout.MAX_COL
        max_col = 0
        # Check at least one FE is on
        fe_active_names=map(lambda rn: rn+"_active",fe_layout.COL_MAP.keys())
        if len(filter(lambda x: x in fe_active_names,kwd.keys())) == 0:
            raise RuntimeError("Need to activate at least one Front End")
        # Not allowing non-consecutive regions
        if 'diff_active' in kwd.keys() and \
                ('sync_active' in kwd.keys() and not 'lin_active' in kwd.keys()):
            raise RuntimeError("Activation of non-consecutive regions not allowed")
        # sensor attached to this ROC
        self._attached_sensor=sensor_geometry
        # Get the maximum range of all active FE
        for active_name,is_active in kwd.iteritems():
            if active_name not in fe_active_names:
                raise ValueError("Not valid FE '{0}'".format(active_name))
            if not is_active:
                continue
            fe_name = active_name.split("_active")[0]
            # Get the minimum intersected value
            min_col = min(min_col,fe_layout.COL_MAP[fe_name][0])
            # Get the maximum intersected value
            max_col = max(max_col,fe_layout.COL_MAP[fe_name][1])
        # Filling static data
        # -- columns 
        self._min_col = min_col
        self._max_col = max_col
        # -- rows
        self._min_row = fe_layout.MIN_ROW+fe_layout.RE
        self._max_row = fe_layout.MAX_ROW-fe_layout.RE
        # -- Correcting and re-mapping if needed
        self._mapping(sensor_geometry)
        # -- And incorporating the pitch 
        self.xpitch = sensor_geometry.xpitch
        self.ypitch = sensor_geometry.ypitch
    
    def columns(self):
        return (self._min_col,self._max_col)
    
    def rows(self):
        return (self._min_row,self._max_row)

    def is_pixel_layout_25by100(self):
        """
        """
        return self._attached_sensor.is25by100()
    
    def __str__(self):
        return '<fe_layout instance: ACTIVATED Columns: ({0},{1}), Rows: ({2},{3})>'.\
                format(self._min_col,self._max_col,self._min_row,self._max_row)

    def _mapping(self,sensor_geometry):
        """Given a Front End layout class, it corrects the
        max and min columns and rows which the FE is considering
        assuming the proper mapping beetwen ROC channel and real pixel
        position. 
        This function is intended to be used for the class only

        XXX: TO BE MODIFY parameter --> NOT NEEDED use _attached_sensor

        Parameters
        ----------
        sensor_geometry: sensor_layout instance
        """
        if sensor_geometry.is25by100():
            # Correction +-2 to create the minimum of 3 rows/columns
            # beyond the edges
            # Remember 50x50: 400 cols, 25x100:200 cols
            self._min_col = int(self._min_col*0.5)+2
            self._max_col = int(self._max_col*0.5)-2
            # Remember 50x50: 192 rows, 25x100:384 cols
            self._min_row = self._min_row*2+2
            self._max_row = self._max_row*2-2
        # Limits should be corrected as well
        # Creating instance attributes MIN and MAX, not the class ones 
        # (a RD53A is always 50x50), but now this ROC instance 
        # has been re-mapped
        (self.MIN_COL,self.MAX_COL) = sensor_geometry.columns()
        (self.MIN_ROW,self.MAX_ROW) = sensor_geometry.rows()

    def redefine_fiducial(self,col_range=None,row_range=None):
        """Re-define the fiducial region, if needed to remove extra
        columns and/or rows from the pre-defined ones. Useful when 
        desaligned beams, inefficient reference sensors, ...
        The region should be defined in columns and rows.

        Parameters
        ----------
        col_range: (int,int)
            The new column range 
        row_range: (int,int)
            The new row range

        Raises
        ------
        RuntimeError:
            Whether both arguments are None, or the limits introduced
            are lower than the original ones
        """
        # Check if a range was introduced and fill with current
        # values those which were not
        if not col_range and not row_range:
            raise RuntimeError("Need at least one region to update")
        elif not col_range:
            col_range = (self._min_col,self._max_col) 
        elif not row_range:
            row_range = (self._min_row,self._max_row) 

        # First check that the new region makes sense
        if col_range[0] < self._min_col \
                or col_range[1] > self._max_col \
                or row_range[0] < self._min_row \
                or row_range[1] > self._max_row:
            message="Invalid fiducial re-definition. Values make non-sense:\n"
            message+=" Initial limits col: ({0:3},{1:3})\n".format(self._min_col,self._max_col)
            message+="  --- Trying to set: ({0:3},{1:3})\n".format(col_range[0],col_range[1])
            message+=" Initial limits row: ({0:3},{1:3})\n".format(self._min_row,self._max_row)
            message+="  --- Trying to set: ({0:3},{1:3})".format(row_range[0],row_range[1])
            raise RuntimeError(message)
        self._min_col = col_range[0]
        self._max_col = col_range[1]
        self._min_row = row_range[0]
        self._max_row = row_range[1]

    def in_fiducial(self,col,row):
        """ Return whether is inside fiducial region or not
        
        Parameters
        ----------
        col: float
            The column position
        row: float
            The row position

        Returns
        -------
        bool: Whether or not the col,row is inside the FE region
        """
        return row > self._min_row and row < self._max_row \
                and col > self._min_col \
                and col < self._max_col 

class hit_mask(object):
    """Defines a mask over the current sensor, taking into account dead pixels 
    as well as noisy pixels.
    """
    def __init__(self,fe,meas_tree,sensorId,rawdata,noisy_freq=None):
        """Defines a mask over the current sensors.
        WARNING: if any hit has been masked during the pre-processing step
        (as the noisy pixels), they appeared here as dead pixels. In order 
        to unfold which is which, the masking should be de-activated during
        the pre-processing steps (Eutelescope code)
        
        In the case of a calibration applied, those pixels with no calibrated
        curves are going to be masked here as well, and they will appeared both
        in the dead_pixel histogram and in a dedicated `calfailed` histogram.

        Parameters
        ----------
        fe: sensor_description.fe_layout instance
            The FE layout to be used in the measure. The fiducial region should 
            be defined already
        meas_tree: ROOT.TTree
            The tree were the raw zero supressed data is
        sensorId: int
            The ID of the DUT
        rawdata: TBTree.pixels
            The instance of the rawdata, to extract the calibration info
        noisy_freq: float
            The relative number of times (with respect the total
            events, that a pixel is fired in order to be considered
            noisy
        """
        # Define the real geometry in a TH2. The histogram will be 
        # filled by the Draw/Project (built-in C++) method
        self._dead_pixels = np.zeros((fe.MAX_COL+1,fe.MAX_ROW+1),dtype=bool)
        self._noisy_pixels = np.zeros((fe.MAX_COL+1,fe.MAX_ROW+1),dtype=bool)

        # XXX  TO DO: use is fiducial to actually use only the fiducial hits
        # Hit matrix describe by { col: {rows, } }
        self._pixel_histo = ROOT.TH2F("all_pixels",";col;row;Entries", 
                (fe.MAX_COL-fe.MIN_COL)+1,fe.MIN_COL,fe.MAX_COL+1,
                (fe.MAX_ROW-fe.MIN_ROW)+1,fe.MIN_ROW,fe.MAX_ROW+1)
        self._dead_pixels_histo = self._pixel_histo.Clone("dead_pixels")
        self._dead_pixels_histo.SetTitle(";col;row;")
        self._noisy_pixels_histo = self._pixel_histo.Clone("noisy_pixels")
        self._noisy_pixels_histo.SetTitle(";col;row;")
        # To control calibration curves problems
        self._calibration_fail_histo = self._pixel_histo.Clone("calfail_pixels")
        self._calibration_fail_histo.SetTitle(";col;row;")
        # Fill histogram and identify dead or noisy pixels
        kk = meas_tree.Project("all_pixels","yPos/{0}:xPos/{1}".format(fe.ypitch,fe.xpitch),
                "sensorId=={0}".format(sensorId))
        nentries = float(self._pixel_histo.GetEntries())
        for col in xrange(fe.MIN_COL,fe.MAX_COL+1):
            for row in xrange(fe.MIN_ROW,fe.MAX_ROW+1):
                if not fe.in_fiducial(col,row):
                    continue
                # Check if the calibration fails for the pixel
                the_bin = self._pixel_histo.FindBin(col,row)                
                if self._pixel_histo.GetBinContent(the_bin) == 0 \
                        or (not rawdata.valid_calibration_pixel(sensorId,col,row)):
                    self._dead_pixels[col][row] = True
                    self._dead_pixels_histo.SetBinContent(the_bin,1)
                    if not rawdata.valid_calibration_pixel(sensorId,col,row):
                        self._calibration_fail_histo.SetBinContent(the_bin,1)
                    # And the first neighbours to this pixel
                    # Do we need extra pixels to be masked?? Yes, otherwise there
                    # are problems with rounding (because telescope resolution)
                    for nb in [(col-1,row-1),(col-1,row),(col-1,row+1),
                            (col,row-1),(col,row+1),
                            (col+1,row-1),(col+1,row),(col+1,row+1)]:
                        try: 
                            self._dead_pixels[nb[0]][nb[1]]=True
                            the_bin_neighbour = self._dead_pixels_histo.FindBin(nb[0],nb[1])
                            self._dead_pixels_histo.SetBinContent(the_bin_neighbour,1)
                        except IndexError:
                            pass
                # XXX ACTIVATE THAT
                #elif float(self._pixels.GetBinContent(col,row))/nentries > max_freq:
                #    self._noisy_pixels[col][row] = True
    
    def is_masked(self,col,row):
        """Check if the pixel is masked or not
        """
        # Out-of-bounds
        if col < 0 or col >= self._dead_pixels.shape[0] or \
                row < 0 or row >= self._dead_pixels.shape[1]:
            return True
        return self._dead_pixels[col][row]
    
    def attach_histos_to_file(self,root_file):
        """Set the relevant histograms defined in this class (all pixels,
        dead pixels and noisy pixels) in the given file. The file has have 
        writting permissions.

        Parameters
        ----------
        root_file: ROOT.TFile
            The output root file where the histos are going to be stored
        """
        for h in [self._pixel_histo,self._dead_pixels_histo,self._noisy_pixels_histo,self._calibration_fail_histo]:
            h.SetDirectory(root_file)

    # FIXME: iterate over an array, just showing dead pixels
    #def __str__(self):
    #    """Info
    #    """
    #    msg="[Dead pixels (col,row)] "
    #    for (col,row) in filter(lambda x: self._dead_pixels:
    #        msg += "({0:3},{1:3}) ".format(col,row)
    #    return msg
