#!/usr/bin/env python
"""Some extra functions, mainly fitting related

XXX DOC

jorge.duarte.campderros@cern.ch IFCA 2019.12.13
"""
__author__ = "Jordi Duarte-Campderros"
__credits__ = ["Jordi Duarte-Campderros"]
__version__ = "0.1"
__maintainer__ = "Jordi Duarte-Campderros"
__email__ = "jorge.duarte.campderros@cern.ch"
__status__ = "Development"

import ROOT
import numpy as np
from scipy.stats import moyal
import random
# --- > problems with the help, appear the ROOT help, 
from sifca_utils import plotting as pl
from sifca_utils.functions import peaks_searcher,erlang_landgaus
from sifca_utils.plotting import create_tripad_plot

from alibavaSkifftools.analysis_functions import fit_langaus


def moyal_root(xmin=0,xmax=20):
    """Use Moyal distribution from scipy into root

    Not the optimal code for running intensively, but
    good enough (and easy) to use with this package
    
    Parameter
    ---------
    x_min: float, the low range
    x_max: float, the high range

    Returns
    -------
    f: TF1, the moyal 
    _m: helper function defining the moyal, returned in order
        to assure is not deleted
    """
    def _m(x,p):
        """
        p = [ loc, sigma, area ]
        """
        return p[2]*moyal(loc=p[0],scale=p[1]).pdf(x[0])

    f = ROOT.TF1('moyal{}'.format(random.getrandbits(32)),_m,xmin,xmax,3)
    f.SetParNames('mu','sigma','norm')
    return f,_m

def search_initial_landau_par(h,window,rd53a):
    """Search the peak for the Landau, if rd53a discard the saturation
    peak

    Parameters
    ----------
    h: ROOT.TH1
        The histogram
    window: float
        Distance form the peak
    rd53a: bool
        Whether or not is a rd53a chip

    Return
    ------
    peak,sigma,x0,x1: tuple(float)
    """
    # -- XXX TO BE DEPRECATED in favor of peak_searcher
    peak_bin=h.GetMaximumBin()
    peak = h.GetBinCenter(peak_bin)
    nloop=0
    batch=ROOT.gROOT.IsBatch()
    while rd53a and abs(peak-15) < 2:
        print ">>> \033[1;33mWARNING ToT estimation\033[1;m:"\
                " Potential problem with this histogram."
        print ">>> \033[1;33mWARNING ToT estimation\033[1;m:"\
                " The extracted peak is suspiciously close to the saturation ToT bin."
        print ">>> \033[1;33mWARNING ToT estimation\033[1;m:"\
                " Trying to find a new one [{}]".format(nloop)
        # Using the TSpectrum class
        # XXX -- Directly used this one?
        ROOT.gROOT.SetBatch(1)
        s=ROOT.TSpectrum()
        nfound = s.Search(h,1,'nobackground new')
        # Get the first peak not being the saturation (coming from the right)
        for i in range(nfound-1,-1,-1):
            peak = s.GetPositionX()[i]
            if abs(peak-15) < 2:
                continue
            break
        nloop+=1
        # Control infinite loop
        if nloop > 10:
            raise RuntimeError("Stop loop! Unable to find a peak")
    # Get the ranges
    sigma = peak/float(window)
    x0 = peak - 2.0*sigma
    x1 = peak + 3.5*sigma

    ROOT.gROOT.SetBatch(batch)

    return peak,sigma,x0,x1

def automatic_moyal_fit(h,window=7.0,rd53a=True):
    """
    Parameters
    ----------
    h: ROOT.TH1
        The histogram
    window: float
        Distance form the peak
    """
    # Get initial values
    peak,sigma,x0,x1 = search_initial_landau_par(h,window,rd53a)

    # Establish initial values
    m,_ = moyal_root(x0,x1)
    m.SetParameters(peak,sigma,h.GetSumOfWeights())
    # And set some ranges to avoid NaN warnings (not problematic but annoying)
    #m.SetParLimits(0,max(0,peak*(1.0-window*sigma)),peak*(1.0+window*sigma))
    m.SetParLimits(1,0,window*peak)
    #m.SetParLimits(2,0,100*h.GetSumOfWeights())
    # And... fit
    batch = ROOT.gROOT.IsBatch()
    ROOT.gROOT.SetBatch(1)
    c = ROOT.TCanvas()
    _=h.Fit(m,'RQ','',x0,x1)
    ROOT.gROOT.SetBatch(batch)    
    # Some cross-checks to avoid meaningfullness results (sigma too small,
    # probably a failure in the fit). If sigma is lower than 5% of the mean
    if m.GetParameter("sigma")/m.GetParameter("mu") <  0.01:
        # In that case use the initial estimation
        m.SetParameter("sigma",sigma)
    return m

def automatic_langaus_fit(h,xmin,xmax,peak_min,peak_max,force_range=False):
    """
    Parameters
    ----------
    h: ROOT.TH1
        The histogram
    xmin: float
        Min x-range
    xmax: float
        Max x-range
    peak_min: float
        Min allowed value where to search for the peak
    peak_max: float
        Max allowed value where to search for the peak
    """
    ## TO BE DEPRECATED
    return fit_langaus(h,xmin,xmax,peak_min,peak_max,force_range)


def automatic_erlang_langaus_fit(h,xmin=None,xmax=None):
    """DOCUMENT: origin erlang
    Parameters
    ----------
    h: ROOT.TH1
        The histogram
    xmin: int|None
    xmax: int|None
    """
    # Get initial values
    # Not provided by the user
    if xmin is None:
        # Get the first bin after a potential cut
        total_entries = float(h.GetEntries())
        for i in range(1,h.GetNbinsX()+1):
            if float(h.GetBinContent(i))/total_entries > 1e-5:
                xmin = h.GetBinCenter(i)
                break
        if xmin is None:
            raise RuntimeError("Not found any x-minimum in the histogram"\
                    " with a bin-content larger than 0.001% of the total."\
                    " This cannot be...")
    if xmax is None:
        xmax = h.GetBinLowEdge(h.GetNbinsX()+1)
    peaks=peaks_searcher(h,xmin,xmax)

    # If more than one peak, the s
    if len(peaks) > 1:
        # Non-depleted region
        peak_nd = peaks[0]
        peak = peaks[1]
    elif len(peaks) == 1:
        # Assuming the searcher only found the lower peak
        peak_nd = peaks[0]
        # The lower peak is assumed to be 30% smaller than the 
        # charge coming from depleted region 
        peak = peak_nd*3.0

    # The functions
    el = ROOT.TF1('erlang_landgaus',erlang_landgaus,xmin,xmax,8)#erlang_landgaus.npars)
    # Initial values 
    # --- The fraction of landgaus vs. erlang
    el.SetParameter(7,0.5)
    el.SetParLimits(7,0,1)
    # --- The landau MPV
    el.SetParameter(0,peak)
    el.SetParLimits(0,peak_nd,peak*4.0)
    # --- The landau width (scale), the average fluctuations around the MPV 
    #      (assuming a 5/10% of the MVP)
    el.SetParameter(1,peak*0.1)
    el.SetParLimits(1,peak*0.01,peak*0.5)
    # --- The normalization (for both Erlang and Landgaus
    el.SetParameter(2,h.GetEntries())
    # --- The gaussian noise (fluctuations due to electronics, assuming 20% )
    el.SetParameter(3,peak*0.2)
    el.SetParLimits(3,peak*0.05,peak_nd)
    # -- The shape of the erlang (closest value to 1, exponential shape; 
    #    largest positive value, gaussian-like shapes)
    el.SetParameter(4,2.0)
    el.SetParLimits(4,0,4)
    # -- The Gamma location: should be 0 for Erlang dist.
    el.FixParameter(5,0.0)
    # -- The Erlang scale, i.e. the average number of measured charge fluctuations generated
    #    by particles crossing the non-depleted region
    el.SetParameter(6,peak_nd)
    el.SetParLimits(6,0,peak)
    
    stptr=h.Fit(el,'RQS','',xmin,xmax)
    ROOT.gROOT.SetBatch(0)

    if int(stptr) != 0:
        print("\033[1;33mErlang+Landau(x)Gaus fit failed!\033[1;m")
        el.status = -1
    else:
        el.status = 0

    return el


class dummy_args():
    """Simple class with some mandatory and not mandatory
    argumentes created from a dict
    """
    def __init__(self,plot_args,institution='CMS',preliminary=False):
        """XXX 

        Parameters
        ----------
        plot_args: dict
            The wrapper to the title elements. Must contain the following
            keys and their values type:
            
            * 'name_sensor': str
            * 'name_layout': str

            Can contain:
            
            * 'temperature': float
            * 'voltage_bias': float
            * fluence': ' float
        institution: str
            The (institutional) name to put in the plots
        preliminary: bool
            Whether or not to include the word `Prelimary` after `institution`
        """
        self.institution=institution
        self.preliminary='Preliminary' if preliminary else ''
        # Mandatory argumens
        mandatory = [ 'name_sensor', 'name_layout' ]
        for attr in mandatory:
            if not plot_args.has_key(attr):
                raise RuntimeError('Mandatory key `{}` for plot_args argument'.format(attr))
            setattr(self,attr,plot_args[attr])
    
        # Initialize to None the non-mandatory
        non_mandatory = [ 'voltage_bias', 'temperature', 'fluence' ]
        for attr in non_mandatory:
            setattr(self,attr,None)

        # Fill extra arguments
        for k,v in filter(lambda (k,v): k not in mandatory,plot_args.iteritems()):
            setattr(self,k,v)

    def create_lines(self):
        """
        """
        HELPVAR = { #'leakage_current': 'I_{leak}=',
                'temperature' : 'T=',
                'voltage_bias': 'V_{bias}=',
                'fluence': ' #Phi_{target}=',
                'name_sensor': '',
                'name_layout':'',
                'fluence_region': ''
                }
        HELPVAR_U = { #'leakage_current': '#muA',
                'temperature' : '^{o}C',
                'voltage_bias': 'V',
                'fluence': 'n_{eq}/cm^{2}',
                'name_sensor': '',
                'name_layout':'',
                'fluence_region': ''
                }
        
        # create the lines to be filled
        lines = { 'line0': self.institution+self.preliminary,
                'line1':self.name_sensor+' ('+self.name_layout+')',
                'line2':''} 
        ordered_lines = { 3: 'voltage_bias',\
                #4: 'leakage_current', 
                5: 'temperature', 6: 'fluence', 7: 'fluence_region', }
        for (i,(_ignore,argname)) in enumerate(ordered_lines.iteritems()):
            if getattr(self,argname) is not None: 
                lines['line2'] += '{0}{1} {2},  '.format(HELPVAR[argname],getattr(self,argname),HELPVAR_U[argname])
        lines['line2'] = lines['line2'][:-3]

        return lines


def prepare_callback(histo,is_25by100,plot_args,preliminary):
    """Prepare format, titles and other cosmethics to create a standarize
    plot. Int the 2D maps, it will create the appropiate canvas dimension
    depending the layout of the sensor.

    Parameters
    ----------
    histo: ROOT.TH
        The histogram to be plotted
    is_25x100: bool
        Whether the sensor layout is 25x100, otherwise assumes 50x50
    plot_args: Namespace
        A list of all arguments needed for writting down the legend
        See `dummy_args` class for the list of arguements
    preliminary: bool
        Whether or not to include the word preliminary after CMS
    
    Returns
    -------
    _callback,title: The function to create the canvas with the histogram inside, and
            the histogram

    Usage Example
    -------------
    >>> func,title_lines = prepare_callback(histogram,is_25by100=True, plots_args=from_a_dict,preliminary=True)
    # Get the canvas (and the associated histogram)
    >>> canvas,h = func()
    # Set the obtained title in the canvas
    >>> canvas,pt=pl.set_external_title(canvas,first_cms_line=True,**lines)
    """
    # Check if it was in batch before
    batch = ROOT.gROOT.IsBatch()
    ROOT.gROOT.SetBatch()
    histo.SetTitle('')
    if histo.GetName() in ['eff_full','tot_full', 'all_pixels','dead_pixels','noisy_pixels']:
        style=pl.set_sifca_style(ratio_1to2=True,stat_off=True)
        histo.UseCurrentStyle()
        # - BE careful, fixing range for electrons CHArge
        if histo.GetName() == 'tot_full' and histo.GetMaximum() > 1e4:
            histo.GetZaxis().SetRangeUser(900,25e3)
        def _callback():
            return pl.prepare_TH2D_at_1to4_plot(histo),histo
    elif is_25by100 \
            and len(filter(lambda x: histo.GetName().find(x) != -1, \
                ['_0dot5','_1','_2','_4','num','den' ])) != 0\
                and histo.GetName().find('adrem_tot') == -1:
        style=pl.set_sifca_style(ratio_1to4=True,stat_off=True)
        if histo.GetName().find('res') != -1:
            ROOT.gStyle.SetPalette(ROOT.kTemperatureMap)
        histo.UseCurrentStyle()
        def _callback():
            return pl.prepare_TH2D_at_1to4_plot(histo),histo
    else:
        style=pl.set_sifca_style(squared=True,stat_off=True)
        histo.UseCurrentStyle()
        def _callback():
            canvas = ROOT.TCanvas()
            histo.Draw("COLZ")
            return canvas,histo

    if preliminary:
        cmspre=' Preliminary'
    else:
        cmspre=''

    lines = dummy_args(plot_args,institution='CMS',preliminary=preliminary).create_lines()

    ROOT.gROOT.SetBatch(batch)
    
    return _callback,lines


def plot_histos(histolist,is_25by100,plot_args,prefixname,preliminary=True):
    """From a list of 1D plots, create them in the same canvas and save the
    figure (as png)

    Parameters
    ----------
    histolist: list(ROOT.TH1F)
        The histogram to be plotted
    is_25x100: bool
        Whether the sensor layout is 25x100, otherwise assumes 50x50
    plot_args: Namespace
        A list of all arguments needed for writting down the legend
        See `dummy_args` class for the list of arguements
    prefixname: str
        The plotname to be prefixed to the histo name title
    preliminary: bool
        Whether or not to include the word preliminary after CMS
    
    Returns
    -------
    plotname: str   
        The created plotname 
    """
    batch = ROOT.gROOT.IsBatch()
    ROOT.gROOT.SetBatch()
    
    # Assign the colors from the current palette
    ncolors = ROOT.gStyle.GetNumberOfColors()
    nhistos = len(histolist)
    dc = int(ncolors/nhistos)
    leg = ROOT.TLegend(0.55,0.5,0.80,0.7)
    for i,h in enumerate(histolist):
        cur_color = ROOT.gStyle.GetColorPalette(i*dc)
        # Remove same for the first one 
        h.SetOption('PE1SAME')
        h.SetLineColor(cur_color)
        h.SetLineStyle(1+2*i)
        h.SetLineWidth(3)
        h.SetMarkerColor(cur_color)
        h.SetMarkerStyle(20+i)
        leg.AddEntry(h,h.GetTitle(),'LP')
        h.SetTitle('')
    # remove SAME for the first one
    histolist[0].SetOption('PE1')

    # Create teh canvas and plot the first one
    histo = histolist[0]
    canvas_function,lines = prepare_callback(histo,is_25by100,plot_args,preliminary)

    ## Get the canvas
    canvas,h = canvas_function()

    ## Set the tilea
    canvas,pt=pl.set_external_title(canvas,first_cms_line=True,**lines)

    # Start the loop of all histos (the first one is already create in the 
    # canvas)
    for _h in histolist[1:]:
        _h.Draw("HPE1SAME")
    leg.Draw()
    plotname = '{0}_{1}.{2}'.format(prefixname,histolist[0].GetName(),plot_args['format'])
    #canvas.Print(plotname.replace('.png','.pdf'))
    #canvas.BuildLegend()
    canvas.SaveAs(plotname)
    ROOT.gROOT.GetListOfCanvases().Delete()
    
    ROOT.gROOT.SetBatch(batch)
    
    return plotname

def plot_histo(histo,is_25by100,plot_args,prefixname,preliminary=True):
    """Plot the histogram in the given format, taking care of the 
    layout and format

    Parameters
    ----------
    histo: ROOT.TH
        The histogram to be plotted
    is_25x100: bool
        Whether the sensor layout is 25x100, otherwise assumes 50x50
    plot_args: Namespace
        A list of all arguments needed for writting down the legend
        See `dummy_args` class for the list of arguements
    prefixname: str
        The plotname to be prefixed to the histo name title
    preliminary: bool
        Whether or not to include the word preliminary after CMS
    
    Returns
    -------
    plotname: str   
        The created plotname 
    """
    batch = ROOT.gROOT.IsBatch()
    ROOT.gROOT.SetBatch()

    canvas_function,lines = prepare_callback(histo,is_25by100,plot_args,preliminary)

    # Get the canvas
    canvas,h = canvas_function()
    # Set the tilea
    canvas,pt=pl.set_external_title(canvas,first_cms_line=True,**lines)
    plotname = '{0}_{1}.{2}'.format(prefixname,histo.GetName(),plot_args['format'])
    canvas.SaveAs(plotname)
    ROOT.gROOT.GetListOfCanvases().Delete()
    
    ROOT.gROOT.SetBatch(batch)
    
    return plotname

def plot_2d_and_prof(histo,is_25by100,plot_args,prefixname,preliminary=True):
    """Plot a 2d histogram along with its profiles in X and Y, taking care of the 
    layout and format

    Parameters
    ----------
    histo: ROOT.TH
        The histogram to be plotted
    is_25x100: bool
        Whether the sensor layout is 25x100, otherwise assumes 50x50
    plot_args: Namespace
        A list of all arguments needed for writting down the legend
        See `dummy_args` class for the list of arguements
    prefixname: str
        The plotname to be prefixed to the histo name title
    preliminary: bool
        Whether or not to include the word preliminary after CMS
    
    Returns
    -------
    plotname: str   
        The created plotname 
    """
    # For the triple pad, it needs to craete an image, therefore
    # it cannot be batch
    #batch = ROOT.gROOT.IsBatch()
    #ROOT.gROOT.SetBatch()

    canvas_function,lines = prepare_callback(histo,is_25by100,plot_args,preliminary)

    # Get the canvas
    canvas,h = canvas_function()
    
    c,_cont = create_tripad_plot(h)

    _cont[0].cd()
    c,pt=pl.set_external_title(c,first_cms_line=True,**lines)
    plotname = '{0}_{1}.{2}'.format(prefixname,histo.GetName(),plot_args['format']) 

    c.cd(0)
    c.SaveAs(plotname)
    ROOT.gROOT.GetListOfCanvases().Delete()
    
    return plotname
