#!/usr/bin/env python
"""Calibration related algorithm.
The module extracts the S-curves for the pixels to obtain the 
calibration constants from an input h5 file created by the BDAQ53 readout
system. 
So far, the input file should be the one obtained from a threshold scan.

The module contains function to plot calibration related quantities.
"""
__author__ = "Jordi Duarte-Campderros"
__credits__ = ["Jordi Duarte-Campderros"]
__version__ = "0.1"
__maintainer__ = "Jordi Duarte-Campderros"
__email__ = "jorge.duarte.campderros@cern.ch"
__status__ = "Development"

import os
from datetime import datetime
import numpy as np
import tables

from progress.bar import Bar

import ROOT

import sifca_utils.plotting as pl

# FIXME: To be centralized in a file/module
# Units and ctes
ADC  = 1
e    = 1.0
kE   = 1e3*e
V    = 1.0
mV   = 1e-3*V
um   = 1.0

Coulomb2E  = 6.24150975e18*e 
Coulomb2kE = Coulomb2E/kE # kilo-electrons 
ePerMicron = 76.0*e/um

# Number of bits available for ToT 
nTOT = 2**4

# XXX weak... 
SENSOR_100by25 = [ 'W3X3Y2','W3X3Y1']

# forcing to deal this sensors as 25x100 
# In order to b
FORCE_100by25 = False

# The default Vref value (in V)
VREF = '0.8'

# Describe where is the last TOT available bit (saturation bit)
TOT_BIT_SATURATION = 14


# Calibration curve for VCal, assuming a capacitance of the
# injection circuit of 8.21+-0.04 fF: https://twiki.cern.ch/twiki/bin/view/RD53/BiasesNDacs53A
# --- For Vref=0.8 V
class QDAC_Conversor(object):
    # A:= conversion factor from DVcal to electrons. [A]=Electrons/DVcal
    _avref08 = 10.1*e/ADC
    # B:= Offset, (noise) [B]=Electrons
    _bvref08 = 50.0*e
    # --- For Vref=0.9 V (see 20200916_CNM3D_DESY2020TB.odp XXX TO BE UPDATED)
    # A:= conversion factor from DVcal to electrons. [A]=Electrons/DVcal
    _avref09 = 11.3*e/ADC
    # B:= Offset, (noise) [B]=Electrons
    _bvref09 = 58.0*e
    def __init__(self,vref='0.8'):
        """The calibration conversor from DVCal to Electrons. Depending
        the Vref used, different linear factors are applied. See details
        at https://twiki.cern.ch/twiki/bin/view/RD53/BiasesNDacs53A 
        and [20200916_CNM3D_DESY2020TB.odp --> XXX PROVISIONAL]

        Parameters
        ----------
        vref: str
            The Vref used in [V]. Valid values are '0.8' (default)
            and '0.9'
        """
        if vref == '0.8':
            self.A = QDAC_Conversor._avref08
            self.B = QDAC_Conversor._bvref08
        elif vref == '0.9':
            print("Assume Vref=0.9 V for DVCal->electrons conversion")
            self.A = QDAC_Conversor._avref09
            self.B = QDAC_Conversor._bvref09
        else:
            raise RuntimeError('Invalid Vref value {}'.format(vref))
        self.err_evcal = 12.0*e

    def __call__(self,vcal):
        return (self.A*vcal+self.B)

# XXX -- Careful with this!! Only valid for the 3D mapping 
#        of FBK and also CNM (At least those from end of 2019 batch)
def roc_to_pixel_map_100by25_3D(col,row):
    """Map indices to convert from ROC -> PIXEL coordinates.
    Given the column and row from the ROC, the function returns
    the corresponding column and row of the pixel bump-bonded to
    that ROC channel.
    
    Parameters
    ----------
    col: int
    row: int

    Returns
    -------
    col_pixel,row_pixel
    
    Notes
    -----
    The function accepts numpy arrays as well.

    """
    return np.floor(col/2).astype(int),(2*row+(col%2))

def get_indices_for_100by25_3D(shape):
    """Creates a matrix with the ROC channel size, where each 
    element of the matrix `m[col,row]` contains a 2-tuple with 
    the corresponding column and row of the pixel bump-bonded to
    that ROC channel

    Parameter
    --------
    shape: (int,int)
        The number of columns and the number of rows of the ROC-chip

    Return
    ------
    numpy.ndarray: the matrix map to the PIXEL coordinates, being the
        shape of the new matrix: (shape[0],shape[1],2)        
    """
    nm = np.zeros((shape[0],shape[1],2),dtype=int)
    # Mapping ROC -> sensor
    for c in range(shape[0]):
        for r in range(shape[1]):
            col,row = roc_to_pixel_map_100by25_3D(c,r)
            nm[c][r] = [col,row]
    return nm

def convert_100by25_3D(roc_matrix):
    """Convert an array on the ROC coordinates into PIXEL coordinates.
    
    Parameter
    --------
    roc_matrix: numpy.ndarray on ROC coordinates

    Return
    ------ 
    numpy.ndarray: on pixel coordinates. The shape of the matrix will
        correspond to (roc_matrix.shape[0]*0.5,roc_matrix.shape[1]*2).
        The channels mapping is defined in the `roc_to_pixel_map_100by25_3D`.

    See also
    --------
    `get_indiced_for_100by25_3D`
    """
    # Obtain the map from the ROC coordinates to the pixel ones
    # just do it once...
    if not hasattr(convert_100by25_3D,'indices'):
        convert_100by25_3D.indices = get_indices_for_100by25_3D(roc_matrix.shape)
    # Prepare the format to feed the final matrix in pixel coordinates
    cmap = convert_100by25_3D.indices[:,:,0]
    rmap = convert_100by25_3D.indices[:,:,1]
    # The matrix converted from ROC to Pixel layout
    final = np.zeros((int(roc_matrix.shape[0]*0.5),int(roc_matrix.shape[1]*2)),dtype=roc_matrix.dtype)
    final[cmap,rmap]=roc_matrix

    return final

# dummy class to be used as container for the calibration curves
class CalibrationContainer(object):
    def __init__(self):
        self._array = None
        self._detected_all = None
        # The fit model 
        self._tot_response = None
        # The measured points
        self._graph = ROOT.TGraphErrors()
        # The calibration model parameters,
        # for ADC and electrons
        self._a = {}
        self._b = {}
        self._c = {}
        self._t = {}
        for wx in ['elec', 'vcal']: 
            self._a[wx] = None
            self._b[wx] = None
            self._c[wx] = None
            self._t[wx] = None

class tot_vcal_scan(object):
    """Get the information and manage the result table of
    a threshold scan. Note that the pixels described in the file
    are converted into sensor layout. 
    XXX -- Currently only FBK 3D conversion is implemented XXX

    The file should contain a table with a `HistTot` Array and
    the configuration parameters (Group) used to perform the scan. 
    
    The configuration group in particular contains the start value
    for the Vcal, the end Vcal value, and the step used in the scan, and
    the number of injections per Vcal step.
    
    The HistTot array contains the array of pixels, repeated per every
    step in the Vcal and for every ToT. Each value is interpreted as 
    the number of times that the particular pixel was activated in the 
    given Vcal step producing the given ToT. 
    """
    def __init__(self,h5file,vref=None):
        """
        Parameters
        ----------
        h5file: str
        vref: str|None
            The Vref used for the DACs. Valid values are '0.8' (default) 
            and '0.9'. 
            If None, it is going to use the content of the global `VREF`
        """
        # The Q(DVcal) conversion
        if not vref:
            self.evcal = QDAC_Conversor(VREF)
        else:
            self.evcal = QDAC_Conversor(vref)

        self._h5 = tables.File(h5file)
        # Check validity
        try: 
            run_config = self._h5.root.configuration.run_config
        except (AttributeError,tables.exceptions.NoSuchNodeError):
            raise RuntimeError("`run_config` not present. Not a valid file '{}'".format(h5file))
        try: 
            self._measurements = np.array(self._h5.root.HistTot)
        except (AttributeError,tables.exceptions.NoSuchNodeError):
            raise RuntimeError("`HistTot` not present. Not a valid file '{}'".format(h5file))
        # Get the metadata info of the scan
        self._injections = int(list(filter(lambda v: v[0] == b'n_injections',run_config.cols))[0][1])
        vcal_min   = int(list(filter(lambda v: v[0] == b'VCAL_HIGH_start',run_config.cols))[0][1])
        vcal_max   = int(list(filter(lambda v: v[0] == b'VCAL_HIGH_stop',run_config.cols))[0][1])
        self._vcal_step  = int(list(filter(lambda v: v[0] == b'VCAL_HIGH_step',run_config.cols))[0][1])
        # More extra info
        self._start_col  = int(list(filter(lambda v: v[0] == b'start_column',run_config.cols))[0][1])
        self._stop_col   = int(list(filter(lambda v: v[0] == b'stop_column',run_config.cols))[0][1])
        self._start_row  = int(list(filter(lambda v: v[0] == b'start_row',run_config.cols))[0][1])
        self._stop_row   = int(list(filter(lambda v: v[0] == b'stop_row',run_config.cols))[0][1])
        # Wait for it ... 
        try:
            self._sensor     = str(list(filter(lambda v: v[0] == b'chip_id',run_config.cols))[0][1]).upper()
        except IndexError:
            # Newer bdaq, where the field change to `chip_sn`
            self._sensor     = str(list(filter(lambda v: v[0] == b'chip_sn',run_config.cols))[0][1]).upper()

        # Get the TDAC Mask, probably will be useful to correlate ranges 
        self._tdac_mask = np.array(self._h5.root.configuration.TDAC_mask)
        
        # Get the enable pixel mask
        self._enable_mask=np.array(self._h5.root.configuration.enable_mask)

        # The threshold map, 
        self._h5_threshold_map = np.array(self._h5.root.ThresholdMap)
        
        # Get the array of injected vcal and the ToT
        self._vcal_injected = np.array(list(range(vcal_min,vcal_max,self._vcal_step)),dtype=int)-vcal_min
        # Get in electrons using the calibration curve for vcal
        self._elec_injected = self.evcal(self._vcal_injected)
        self._tot_values    = np.array(list(range(nTOT)),dtype=int)
        
        # Re-map the ROC to be accomodated into pixels units
        if FORCE_100by25 or (self._sensor in SENSOR_100by25):
            self._tdac_mask = convert_100by25_3D(self._tdac_mask)
            self._enable_mask=convert_100by25_3D(self._enable_mask)
            self._h5_threshold_map = convert_100by25_3D(self._h5_threshold_map)
            
            self._start_col,_  = roc_to_pixel_map_100by25_3D(self._start_col,0)
            self._stop_col,_   = roc_to_pixel_map_100by25_3D(self._stop_col,0)
            _,self._start_row  = roc_to_pixel_map_100by25_3D(0,self._start_row)
            _,self._stop_row   = roc_to_pixel_map_100by25_3D(0,self._stop_row)
            # Adapt as well the HisToT
            # Extract the number of cols and rows 
            ncols = self._enable_mask.shape[0]
            nrows = self._enable_mask.shape[1]
            # And all other maps
            prov_aux = np.zeros(shape=(ncols,nrows,self._measurements.shape[2],self._measurements.shape[3]))
            with Bar("All data is being converted from ROC -> PIXEL coordinates",
                    max=(self._measurements.shape[2]*self._measurements.shape[3]),
                    suffix='%(percent)d%%') as bar:
                for dvcal in range(self._measurements.shape[2]):
                    for totbin in range(self._measurements.shape[3]):
                        prov_aux[:,:,dvcal,totbin] = convert_100by25_3D(self._measurements[:,:,dvcal,totbin])
                        bar.next()
                self._measurements = prov_aux
        
        ## Memoizers for the extracted data for the pixels
        self._pixels_meas = {}
        self._cal_curves  = {}

        # Some messages to show once finished
        self._messages = []

    @property
    def start_col(self):
        """The first column of the measured pixels
        """
        return self._start_col
    
    @property
    def stop_col(self):
        """The last column of the measured pixels
        """
        return self._stop_col

    @property
    def start_row(self):
        """The first rowof the measured pixels
        """
        return self._start_row
    
    @property
    def stop_row(self):
        """The last row of the measured pixels
        """
        return self._stop_row

    def print_messages(self):
        if len(self._messages) > 0:
            print("Captured messages:")
            for m in self._messages:
                print(m)
            # initialize again
            self._messages = []


    def close_file(self):
        """XXX
        """
        self._h5.close()
    
    def has_calibration_curve(self,col,row):
        """Whether or not the obtained curve for ToT vs. elec/VCal is filled
        """
        return self._cal_curves[(col,row)]._present

    def get_measurements_matrix(self,pix_col,pix_row):
        """Extract the measurement matrix for the given pixel. 
        How many times a ToT was measured when a Vcal was injected: [VCal,ToT]
        Note that the total sum of measured ToT for a given VCal must be
        equal or lower than n_injections: sum([n0,n1,n2,...,n_nTOT]) <= n_injected

        Parameters
        ----------
        pix_col: int
        pix_row: int

        Return
        ------
        pixel: np.array (shape=(Vcal_max-Vcal_being)/Vcal step,nTOT)
            How many times a ToT was measured for a given Vcal (over the
            total n_injections)        
        """
        assert isinstance(pix_col,int) and isinstance(pix_row,int),"Arguments must be integers"
        if self._pixels_meas.has_key((pix_col,pix_row)):
            return self._pixels_meas[(pix_col,pix_row)]
        # Find if the pixel is outside the scan limits
        if self._start_col > pix_col > self._stop_col:
            # Empty (FIXME: Maybe a None)
            self._pixels_meas[(pix_col,pix_row)] = np.zeros(shape=(self._vcal_step,nTOT),dtype=int)
        else:
            # Obtain the matrix for all the Vcal steps and measured ToT
            self._pixels_meas[(pix_col,pix_row)] = self._measurements[pix_col,pix_row,:,:]

        return self._pixels_meas[(pix_col,pix_row)]

    def calibration_curve(self,col,row):
        """Obtain the calibration curve for the given pixel, and perform 
        the fit to extract the slope and offset. The information is store in
        a `CalibrationContainer` object (the array with all measurements, 
        the graph with averaged measurements, and the slope and offset of
        the fit function

        Parameters
        ----------
        col: int
        row: int

        Return
        ------
        list((float,float)) (len=n_injection)
            The list contains the average ToT measured (and its dispersion)
            for each VCal injected
        """
        assert isinstance(col,int) and isinstance(row,int),"Arguments must be integers"
        if self._cal_curves.has_key((col,row)):
            return self._cal_curves[(col,row)]._array

        # Create the Calibration Container (
        self._cal_curves[(col,row)] = CalibrationContainer()

        # Find if the pixel is outside the scan limits
        if self._start_col > col > self._stop_col:
            # Empty (FIXME: Maybe a None)
            self._cal_curves[(col,row)]._array = np.zeros(shape=self._vcal_injected.shape[0],dtype=int)
            return self._cal_curves[(col,row)]._array

        # For a given pixel [col,row, :, : ], the matrix describes how many times 
        # a ToT was measured for a given VCal value (see `get_measurements_matrix`)
        m = self.get_measurements_matrix(col,row)
        # To evaluate the average ToT for a given VCal injection, we obtain 
        # how many times a ToT was measured over the total injections (n_injection): 
        # this will define the weights for the ToT
        weights = lambda i_vcal: m[i_vcal,:]/float(self._injections)
        # And evalute all the VCal 
        self._cal_curves[(col,row)]._array = np.zeros(shape=(self._vcal_injected.shape[0],2))
        self._cal_curves[(col,row)]._detected_all = np.zeros(shape=(self._vcal_injected.shape[0]),dtype=bool)
        for i,vcal in enumerate(self._vcal_injected):
            current_weights = weights(i)
            try:
                average_tot = np.average(self._tot_values,weights=current_weights)
                std_tot = np.sqrt(np.average((self._tot_values-average_tot)**2.0,weights=current_weights))
            except ZeroDivisionError:
                average_tot = 0.0
                std_tot = 0.0
            self._cal_curves[(col,row)]._array[i] = average_tot,std_tot
            # Mark if was detected all the injections
            sum_weights =  np.sum(current_weights)
            self._cal_curves[(col,row)]._detected_all[i] = (sum_weights >= 1.0)
            # XXX -- We can extract this info as well in a dedicated function, just
            #        to obtain the cross-talk effect...
            if sum_weights > 1.0:
                self._messages.append('Cross-talk indication: Pixel {} {}, '\
                        'sum of all entries > total injection'.format(col,row))
        # The curve to extract the slope and offset
        # Obtain the graphs and extract the calibration constant
        # get a shorter name
        gr = self._cal_curves[(col,row)]._graph
        x = self._vcal_injected
        gr.SetName('cal_curve_{}_{}'.format(col,row))
        gr.SetMarkerStyle(20)
        gr.SetMarkerSize(1.)
        # Take the diference between points as uncertainty in x
        dx = x[1]-x[0]
        i_gr = 0
        for i,(val,val_err) in enumerate(self._cal_curves[(col,row)]._array):
            # Just include those points which detected all the injections
            if not self._cal_curves[(col,row)]._detected_all[i]:
                continue
            gr.SetPoint(i_gr,x[i],val)
            gr.SetPointError(i_gr,dx,val_err)
            i_gr+=1
        # Fit a function defined at  [1] to extract the calibration constant. 
        # [1] https://cds.cern.ch/record/2649493/files/CLICdp-Note-2018-008.pdf
        # 
        # Given that the ToT is a discrete quantity, n-Heaviside functions per 
        # each ToT can the response. Actualy n-activation functions
        # with their offset in the ToT = N, and the middle point giving the average
        # value to change of ToT
        try:
            # XXX - TBD
            xmin_ind = max(0,np.where(self._cal_curves[(col,row)]._array > 0)[0][0]-1)
        except IndexError:
            # This is a full zero array, just mark it. Meaning no ToT was measured
            # for all the vcal steps
            self._messages.append('Not working: Pixel {} {}, no ToT was measured '\
                    'for all the VCal steps (each step n-injections)'.format(col,row))
            self._cal_curves[(col,row)]._present = False
            return self._cal_curves[(col,row)]._array

        # Assume the threshold: take from the midpoint between the last point with 0 response
        # and the first one with 100% injections 
        try:
            last_point_0    = np.where(self._cal_curves[(col,row)]._array[:,0] == 0)[0][-1]
        except IndexError:
            last_point_0 = 0
            self._messages.append('Probably noise: Pixel {} {}, all VCal steps were '\
                    'able to measure all the injection, meaning no threshold was found'.format(col,row))
        try:
            first_point_100 = np.where(self._cal_curves[(col,row)]._detected_all)[0][0]
        except IndexError:
            # Not all of them, let's tag it as failed pixel
            self._enable_mask[col,row] = False
            self._messages.append('Probably inefficient: Pixel {} {}, no VCal steps were '\
                    'able to measure all the injections, meaning injected charge is lost'.format(col,row))
            self._cal_curves[(col,row)]._present = False
            return self._cal_curves[(col,row)]._array
        midpoint = np.mean([x[first_point_100],x[last_point_0]])
        std_midpoint = np.std([x[first_point_100],x[last_point_0]])

        # Check if there is saturation, and exclude those points from the fit (otherwise, the fit model
        # below needs to be changed) 
        try:
            # XXX -- TODO Check more than just the first point, to be sure it's not an outlier
            first_point_saturation = np.where(self._cal_curves[(col,row)]._array[:,0] == TOT_BIT_SATURATION)[0][0]
            maxpoint = x[first_point_saturation]
        except IndexError:
            maxpoint = x.max()*1.1
        
        # Response of the Tot when injecting charge:
        # https://cds.cern.ch/record/2649493/files/CLICdp-Note-2018-008.pdf
        # 
        # The ToT response model a*vcal+b-c/(vcal-t)
        self._cal_curves[(col,row)]._tot_response = p1 = \
                ROOT.TF1('tot_vcal_{}_{}'.format(col,row),'[0]*x+[1]-[2]/(x-[3])',midpoint, maxpoint)
        # Set the threshold from the midpoint between the last injection with 
        # no response at all and the first one with all injections with ToT measured
        # Assigned as error the distance 
        p1.SetParameter(3,midpoint)
        # -- Fit stability is complicated if not fixed this parameter
        p1.SetParLimits(3,midpoint,midpoint)
        #Initialize the others
        # -- The slope of the linear part
        p1.SetParameter(0,0.001)
        p1.SetParLimits(0,0,1)
        # -- The offset of the linear part
        p1.SetParameter(1,0)
        # -- The curvature of the non-linear part
        p1.SetParameter(2,40)
        p1.SetParLimits(2,20,1e3)
        # Convergence:  -- 
        st=gr.Fit(p1,'RQ')
        
        # The slope and offset with its errors (are they those, should I include any other?)
        self._cal_curves[(col,row)]._present = True

        # Assigned as error the distance (do it after the fit, otherwise will be 0)
        p1.SetParError(3,std_midpoint)
        # Assuring the internal function as well
        try:
            gr.GetListOfFunctions()[0].SetParError(3,std_midpoint)
        except AttributeError:
            # XXX - Should I do that?? 
            self._messages('Fit problem: Pixel {} {}, calibration curve was not fitted'.format(col,row))
        
        # Store explicitely the fitted parameters of the ToT response model
        avcal = self._cal_curves[(col,row)]._a['vcal'] = (p1.GetParameter(0),p1.GetParError(0))
        bvcal = self._cal_curves[(col,row)]._b['vcal'] = (p1.GetParameter(1),p1.GetParError(1))
        cvcal = self._cal_curves[(col,row)]._c['vcal'] = (p1.GetParameter(2),p1.GetParError(2))
        tvcal = self._cal_curves[(col,row)]._t['vcal'] = (p1.GetParameter(3),p1.GetParError(3))

        # Converting calibration curve from vcal to electrons
        # 
        #   electrons= A*vcal+B (the evcal function)
        # 
        # The ToT response model a*vcal+b-c/(vcal-t), in electrons become
        # 
        #    a_elec:= a/A       [ToT/electrons]
        #    b_elec:= b - aB/A  [ToT]
        #    c_elec:= c*A       [electrons]
        #    t_elec:= B+t*A     [electrons]
        self._cal_curves[(col,row)]._a['elec'] = (avcal[0]/self.evcal.A,avcal[1]/self.evcal.A)
        self._cal_curves[(col,row)]._b['elec'] = (bvcal[0]-avcal[0]*self.evcal.B/self.evcal.A,
                np.sqrt(bvcal[1]**2.+(self.evcal.B/self.evcal.A*bvcal[1])**2.))
        self._cal_curves[(col,row)]._c['elec'] = (cvcal[0],cvcal[1]*self.evcal.A)
        self._cal_curves[(col,row)]._t['elec'] = (tvcal[0]*self.evcal.A+self.evcal.B,tvcal[1]*self.evcal.A)
        
        return self._cal_curves[(col,row)]._array

    def store_calibration_curves(self,fname=None):
        """Store all the calibration curves graphs into a root file

        Parameters
        ----------
        fname: str|None
            The name of the root output file
        """
        if fname:
            rootfname = fname
        else:
            rootfname = '{}_{}_calibration_constants.root'.format(
                    self._sensor,datetime.now().strftime('%Y%m%d_%H%M%S'))
        f = ROOT.TFile.Open(rootfname,'RECREATE')

        with Bar("Store calibration curves",
                max=(self.stop_col-self.start_col)*(self.stop_row-self.start_row),
                suffix='%(percent)d%%') as bar:
            for ccont in self._cal_curves.values():
                ccont._graph.Write()
                bar.next()
            # Actually writing to the file
            f.Close()
    

    def plot_calibration(self,col,row,fmt='png',ghistos=None):
        """Plot the calibration curve for the pixel

        Parameters
        ----------
        col: int
            Pixel column
        row: int
            Pixel row
        fmt: str
            The plot suffix [Default: png]
        ghistos: list(ROOT.TH1F)|None
            If present the global histograms for the parameters. 
            Note, the a (slope) and b (offset) parameters are 
            expected to store the inverse of them            
        """
        style=pl.set_sifca_style(squared=True,stat_off=True)
        style.cd()
        batch=ROOT.gROOT.IsBatch()
        ROOT.gROOT.SetBatch()
        # Get the curves and build the average function
        cc = self.calibration_curve(col,row)
        # Check if the pixel is calibrated
        if not self.has_calibration_curve(col,row):
            return
        
        # The x-axes
        xvcal = self._vcal_injected
        xvcal_title = '#DeltaV_{cal}'
        
        # Prepare the plot. Fixe the frame
        c0 = ROOT.TCanvas()
        xmin,xmax = xvcal.min()*0.9,xvcal.max()*1.1
        ymin,ymax = 0.0,np.max(cc[:,0]+cc[:,1])*1.15
        h = c0.DrawFrame(xmin,ymin,xmax,ymax)
        h.SetTitle(';{};<ToT> (over {}-injections)'.format(xvcal_title,self._injections))
        # Prepare the two axis
        elec_axis = prepare_electron_axis((xmin,xmax),ymin,h,style)
        # Plot all elements
        h.Draw()
        elec_axis.Draw()
        
        # Function to print both vcal and electron values
        vstr1f = lambda valerr:"{"+'{:.1f}#pm{:.1f}'.format(*valerr)+"}"
        vstr0f = lambda valerr:"{"+'{:.0f}#pm{:.0f}'.format(*valerr)+"}"
        both_units = "#dot^{#DeltaV_{cal}}_{Elecs.}"
        
        # Extract some parameters to print them out
        # a and threshold -------------------------
        slope = self._cal_curves[(col,row)]._a['vcal']
        inv_slope = (slope[0]**-1,slope[0]**-2*slope[1])
        slope_elec = self._cal_curves[(col,row)]._a['elec']
        inv_slope_elec = (slope_elec[0]**-1,slope_elec[0]**-2*slope_elec[1])
        
        threshold = self._cal_curves[(col,row)]._t['vcal']
        threshold_elec = self._cal_curves[(col,row)]._t['elec']
        # Print the fit output
        ypos= 0.80
        xpos=0.45
        lp1=ROOT.TLatex()
        lp1.DrawLatexNDC(0.1,0.92,'Pixel: {}_{} {}_{}'.format(col,'{col}',row,'{row}'))
        lp1.DrawLatexNDC(0.55,0.92,'TDAC MASK: {:.0f}'.format(self._tdac_mask[col,row]))
        lp1.DrawLatexNDC(xpos,ypos,'1/a=^{}_{} #left[{}/ToT#right]'.format(vstr0f(inv_slope),vstr0f(inv_slope_elec),both_units))
        #lp1.DrawLatexNDC(xpos,ypos-1*0.06,'1/b=^{}_{} #left[{}#right]'.format(vstr1f(inv_offset),vstr1f(inv_offset_elec),both_units))
        lp1.DrawLatexNDC(xpos,ypos-0.06,'x_{}=^{}_{} #left[{}#right]'.format('{thr}',vstr0f(threshold),vstr0f(threshold_elec),both_units))
        lp1.DrawLatexNDC(xpos-0.3,ypos,'ax + b - #frac{}{}'.format('{c}','{x-x_{trh}}'))

        # Threshold and uncertainty
        thr_err = ROOT.TBox(threshold[0]-threshold[1],0,threshold[0]+threshold[1],h.GetMaximum())
        #thr_err.SetFillColor(ROOT.kGray)
        thr_err.SetFillColorAlpha(ROOT.kGray,0.5)
        thr_err.Draw('F')

        thr = ROOT.TLine(threshold[0],0,threshold[0],h.GetMaximum())
        thr.SetLineWidth(2)
        thr.SetLineColor(ROOT.kRed+3)
        thr.Draw()
        # Also a label
        lp1.DrawLatex((threshold[0]+threshold[1])*1.05,h.GetMaximum()*0.95,'x_{thr}')

        # And the curve
        self._cal_curves[(col,row)]._graph.Draw('PE1')

        c0.SaveAs('{}_calibration_tot_pixel_{}_{}.{}'.format(self._sensor,col,row,fmt))
        
        # Store the constants if required
        # XXX FIXME -- Now needed two axis as well ??
        if ghistos:
            curvature = self._cal_curves[(col,row)]._c['vcal']
            curvature_elec = self._cal_curves[(col,row)]._c['elec']
            # The histograms are expected to be ordered like the parameters
            for i,(v,verr) in enumerate([inv_slope,inv_offset,curvature,threshold]):
                ghistos[i].Fill(v)

        # Return same ROOT conditions
        ROOT.gROOT.SetBatch(batch)
        ROOT.gROOT.GetListOfCanvases().Delete()


    def dump_calibration_file(self):
        """Create a file with the extracted a,b,c and t parameters
        for the calibration curves, fit to the response function: ax+b-c/(x-t)
        in ADC and electrons units. The electrons units have been converted
        by using the `evcal` function

        Return
        ------
        filename: (str,str)
            The names of the created files (vcal and electrons)
        """
        # XXX - Need split elec/vcal or just create it in teh same matrix
        #       and therefore, used with the same class
        # XXX 

        # First store it in an numpy array (a table maybe?)
        # Obtain the chip size from the tdac mask
        # The matrix is 4 times the ROC matrix, one for each parameter: a,b,c and t
        calibration_constants      = np.zeros(shape=(4,self._tdac_mask.shape[0],self._tdac_mask.shape[1]))
        calibration_constants_elec = np.zeros(shape=(4,self._tdac_mask.shape[0],self._tdac_mask.shape[1]))
        # Be sure the evaluation and the fitting is done,
        not_calibrated_pixels = []
        with Bar("Obtaining calibration curves",
                max=(self.stop_col-self.start_col)*(self.stop_row-self.start_row),
                suffix='%(percent)d%%') as bar:
            for col in range(self.start_col,self.stop_col):
                for row in range(self.start_row,self.stop_row):
                    # Be sure is an active pixel
                    if not self._enable_mask[col,row]:
                        bar.next()
                        continue
                    _ = self.calibration_curve(col,row)
                    # Now the constants are present, 
                    try: 
                        calibration_constants[0][col,row] = self._cal_curves[(col,row)]._a['vcal'][0]
                        calibration_constants_elec[0][col,row] = self._cal_curves[(col,row)]._a['elec'][0]
                    except TypeError:
                        not_calibrated_pixels.append((col,row))
                        bar.next()
                        continue
                    calibration_constants[1][col,row] = self._cal_curves[(col,row)]._b['vcal'][0]
                    calibration_constants_elec[1][col,row] = self._cal_curves[(col,row)]._b['elec'][0]
                    calibration_constants[2][col,row] = self._cal_curves[(col,row)]._c['vcal'][0]
                    calibration_constants_elec[2][col,row] = self._cal_curves[(col,row)]._c['elec'][0]
                    calibration_constants[3][col,row] = self._cal_curves[(col,row)]._t['vcal'][0]
                    calibration_constants_elec[3][col,row] = self._cal_curves[(col,row)]._t['elec'][0]
                    bar.next()
        if len(not_calibrated_pixels) > 0:
            print("List (col,row) of non-calibrated ACTIVE pixels (please take a look):")
            print(str(not_calibrated_pixels).replace('[','').replace(']',''))

        # Ready to create the file
        # XXX - Maybe could be nice to have it in the original h5 file?
        filenames = []
        for calmat,which in [ (calibration_constants,'vcal'),(calibration_constants_elec,'elec') ]:
            filename = '{}_{}_calibration_constants_{}.npy'.format(self._sensor,datetime.now().strftime('%Y%m%d_%H%M%S'),which)
            np.save(filename,calmat,fix_imports=True)
            filenames.append(filename)

        return filenames[0],filenames[1]

    def cross_reference_threshold_tdac(self,estimated_thr_file,force=False):
        """Obtain a 2-dim matrix with the Vthr (in DVcal/elec) and the corresponding 
        TDAC value for each pixel

        Parameters
        ----------
        estimated_thr_file: str|None
            If None, the threshold to be used is extracted from the h5 file, 
            provided by the `cross_reference_threshold_tdac` function 
            (i.e. ThresholdMap array). If a string is provided, it should be
            the name of the calibration constant file created previously with
            the `dump_calibration_file` function, meaning, estimated with the 
            calibration curve fits.
        force: bool
            Whether or not do the calculation, even if there is a memoized
            version of the array. 

        Return
        ------
        numpy.array: The pixels matrix where each pixel contains a 2-dim 
                     vector [ Vthreshold, TDAC ] ) 

        Raises
        ------
        RuntimeError: whenever a wrong type is introduced in the first argument 
                      (estimated_thr_file). Valid types are None (then it will
                      use the ThresholdMap array from the h5 file), and a filename
                      (str), which must be a npy file obtained previously 
                      with the `dump_calibration_file` method

        TypeError:   whenever neither 'elec' nor 'vcal' was introduced as 
                     `which_x` argument
        """
        if not force and\
                (hasattr(self,'_threshold_tdac') and self._threshold_tdac.has_key(which_x)):
            return self._threshold_tdac[which_x]

        # Check what threshold to use
        if estimated_thr_file is None:
            thr_matrix = self._h5_threshold_map
            method = 'scan'
        elif type(estimated_thr_file) == str:
            thr_matrix = apply_calibration(estimated_thr_file).t
            method = 'fits'
        else:
            raise RuntimeError('Invalid argument provided to `estimated_thr_file`'\
                    ' None or string (filename) are the only valid types')
        
        # The new attribute to incorporate into the instance
        self._threshold_tdac = np.zeros(shape=(2,self._tdac_mask.shape[0],self._tdac_mask.shape[1]))
        for col in range(self.start_col,self.stop_col):
            for row in range(self.start_row,self.stop_row):
                self._threshold_tdac[0][col,row]=thr_matrix[col,row]
                self._threshold_tdac[1][col,row]=self._tdac_mask[col,row]
                # Equivalent?? 
                #self._threshold_tdac[0] = thr_matrix
                #self._threshold_tdac[1] = self._tdac_mask
                # Include the electrons self._threshold_tdac[1] (move 1 -> 2)
        return self._threshold_tdac 
        # A masked array ?
        # np.ma.masked_array(self._threshold_tdac,
        #        [~np.array(self._enable_mask),~np.array(self._enable_mask)])

    def candleplot_threshold_tdac(self,which_x='elec',fmt='png',estimated_thr_file=None):
        """Creates and plot a TGraphAsymmErrors containing the average Vthreshold
        corresponding to a given TDAC value. By using all the pixels with the same
        TDAC, a CANDLE PLOT is obtained and for those where the median is far away from
        the mean, the MPV value is used as estimator of the threshold value for that bin.
    
        Paramters
        ---------
        which_x: str
            What x-axis used, either `elec` or `vcal` [Default: elec]
        fmt: str
            The plot suffix [Default: png]
        estimated_thr_file: str|None
            If None, the threshold to be used is extracted from the h5 file, 
            provided by the `cross_reference_threshold_tdac` function 
            (i.e. ThresholdMap array). If a string is provided, it should be
            the name of the calibration constant file created previously with
            the `dump_calibration_file` function, meaning, estimated with the 
            calibration curve fits.
    
        Return
        ------
        ROOT.TGraphAsymmErrors
        """
        # XXX - Need which_x or just create double axis?
        # XXX as argument? 
        # The relative distance between the mean and the median to be 
        # considered equals
        TOLERANCE=0.009

        thrs_tdac = self.cross_reference_threshold_tdac(estimated_thr_file,force=True)
        if estimated_thr_file is None:
            method = 'scan'
            if which_x == 'elec':
                # This will be placin
                aux = self.evcal(thrs_tdac[0])
                thrs_tdac[0] = aux
        else:
            method = 'fits'
        
        # Plotting style and relative stuff 
        style=pl.set_sifca_style(squared=True,stat_off=True)
        style.cd()
        batch=ROOT.gROOT.IsBatch()
        ROOT.gROOT.SetBatch()
        # Create the 2D Histogram and fill it
        thrs_tdac_ma = np.ma.masked_array(thrs_tdac,mask=[~np.array(self._enable_mask),~np.array(self._enable_mask)])
        h = ROOT.TH2F('histo_TDAC_meanThreshold_in_{}'.format(which_x),';TDAC;Threshold [{}]'.format(which_x),
                16,-0.5,15.5,200,thrs_tdac_ma[0].min()*.95,thrs_tdac_ma[0].max()*1.05)
        h.SetMarkerColor(1)
        h.SetLineColor(1)
        h.SetLineWidth(2)
        h.SetFillColorAlpha(ROOT.kGray,0.5)
        for col in range(self.start_col,self.stop_col):
            for row in range(self.start_row,self.stop_row):
                _ = h.Fill(thrs_tdac[1][col,row],thrs_tdac[0][col,row])
        
        # The graph using Quartile info: 
        # And extra graph would be needed to fit it using the number of pixels 
        # as weights
        gr = ROOT.TGraphAsymmErrors()
        gr.SetName('TDAC_meanThreshold_in_{}'.format(which_x))
        # Get the curve from the file
        gr.UseCurrentStyle()
        gr.SetMarkerStyle(20)
        gr.SetMarkerSize(1.0)
        gr_fit = gr.Clone("{}_forfit".format(gr.GetName()))
        # Check is there
        # The x-axis
        if which_x == 'elec':
            ytitle = 'Threshold [electrons]'
        elif which_x == 'vcal':
            ytitle = 'Threshold [#DeltaV_{cal}]'
        else:
            raise RuntimeError('Invalid x-axis name: "{}"'.format(which_x))
        # Let's start the loop over the TDACs
        ymin = 1e10 
        ymax = -1e10
        
        total_tdacs=16
        # Use the entries as weights for the fit, using it as errors
        # less error, more weigth to the fit
        eweights = np.zeros(total_tdacs)
        total_entries=h.GetEntries()
        # Store the mean to calculated afterwards the distance
        measured = np.zeros(total_tdacs)
        for tdac in range(h.GetNbinsX()):
            # Obtain the quantiles in order to evaluate the asymmetric uncertainties
            # 0.1586=1sigma
            htdac=h.ProjectionY("{}_tdac_{}".format(h.GetName(),tdac),tdac+1,tdac+1)
            quantiles = np.array([0.02,0.1586,0.25,0.5,0.75,1.-0.1586,0.98])
            x_q = np.array([0.0,0.0,0.0,0.0,0.0,0.0,0.0])
            _=htdac.GetQuantiles(len(x_q),x_q,quantiles)
            # Whenever the mean of the distribution is not the median (plus an 
            # uncertainty), asumming the gaussian is skew, use the Most 
            # probable value (MPV) as the estimator of the threshold, and the 
            # asymmetric errors are obtained from the extreme quantiles 
            if np.abs(x_q[3]-htdac.GetMean())/x_q[3] > TOLERANCE:
                # MPV
                mean = htdac.GetBinCenter(htdac.GetMaximumBin())
            else:
                mean = x_q[3]
            std_low = mean-x_q[1]
            std_high = x_q[-2]-mean
            gr.SetPoint(tdac,tdac,mean)
            gr.SetPointEYlow(tdac,std_low)
            gr.SetPointEYhigh(tdac,std_high)
            # For the weighting fit
            gr_fit.SetPoint(tdac,tdac,mean)
            measured[tdac] = mean
            # The larger entries, the larger weight of this point for the fit
            eweights[tdac] = (1.0-(htdac.GetEntries()/total_entries))/(total_tdacs-1)
            gr_fit.SetPointError(tdac,0,0.0,eweights[tdac],eweights[tdac])
            if ymin > mean-std_low:
                ymin = mean-std_low
            if ymax < mean+std_high:
                ymax = mean+std_high

        # Fit the function using the number of entries as weights
        w_pol = ROOT.TF1("w_pol_{}".format(which_x),"cheb1",0,15)
        #w_pol.SetLineWidth(2)
        st = gr_fit.Fit(w_pol,"RQSE")
        # To evaluate the error in the parameters, let's take the maximum
        # distance between the estimation and the point, again weighted by the entries
        estimated = np.array(map(lambda x: w_pol(x),range(16)))
        weighted_distance = np.sqrt(eweights*(estimated-measured)**2)
        max_weighted_distance = weighted_distance.max()
        slope_diffs = weighted_distance/np.array(range(16))
        max_slope_diff = slope_diffs[slope_diffs != np.inf].max()
        
        # Draw teh candle plot
        c0 = ROOT.TCanvas()
        h.Draw("CANDLEX(003000310)")
        c0.SaveAs('{}_threshold_per_tdacs_candle_{}_{}.{}'.format(self._sensor,which_x,method,fmt))

        # Prepare the plot. Fixed Frame
        c1 = ROOT.TCanvas()
        hgr = c1.DrawFrame(-0.6,ymin*0.8,15.4,ymax*1.2)
        hgr.SetTitle(";{};{}".format(h.GetXaxis().GetTitle(),h.GetYaxis().GetTitle()))
        hgr.Draw()
        gr.Draw("PE1")
        w_pol.Draw("SAME")
        lp1=ROOT.TLatex()
        lp1.DrawLatexNDC(0.2,0.6,'slope: {:.0f} #pm {:.0f} [{}/TDAC]'.format(w_pol.GetParameter(1),max_slope_diff,which_x))
        lp1.DrawLatexNDC(0.2,0.55,'const: {:.0f} #pm {:.0f} [{}]'.format(w_pol.GetParameter(0),max_weighted_distance,which_x))
        c1.SaveAs('{}_tdac_vs_threshold_{}_{}.{}'.format(self._sensor,which_x,method,fmt))
    
        # Return same ROOT conditions
        ROOT.gROOT.SetBatch(batch)
        ROOT.gROOT.GetListOfCanvases().Delete()

        return gr

    def plot_threshold_tdac(self,which_x='elec',fmt='png',estimated_thr_file=None):
        """Creates and plot a TGraphErrors containing the average Vthreshold
        corresponding to a given TDAC value. By using all the pixels with the same
        TDAC, a gaussian function is fitted, associating the error bars to the 
        estimated sigma. A plot is created as well.
    
        Paramters
        ---------
        which_x: str
            What x-axis used, either `elec` or `vcal` [Default: elec]
        fmt: str
            The plot suffix [Default: png]
        estimated_thr_file: str|None
            If None, the threshold to be used is extracted from the h5 file, 
            provided by the `cross_reference_threshold_tdac` function 
            (i.e. ThresholdMap array). If a string is provided, it should be
            the name of the calibration constant file created previously with
            the `dump_calibration_file` function, meaning, estimated with the 
            calibration curve fits.
    
        Return
        ------
        ROOT.TGraphAsymmErrors
        """
        # XXX - Need which_x or just create double axis?
        thrs_tdac = self.cross_reference_threshold_tdac(estimated_thr_file,force=True)
        if estimated_thr_file is None:
            method = 'scan'
            if which_x == 'elec':
                # This will be placin
                aux = self.evcal(thrs_tdac[0])
                thrs_tdac[0] = aux
        else:
            method = 'fits'
        
        # Plotting style and relative stuff 
        style=pl.set_sifca_style(squared=True,stat_off=True)
        style.cd()
        batch=ROOT.gROOT.IsBatch()
        ROOT.gROOT.SetBatch()
        # The graph
        gr = ROOT.TGraphAsymmErrors()
        gr.SetName('TDAC_meanThreshold_in_{}'.format(which_x))
        # Get the curve from the file
        gr.UseCurrentStyle()
        gr.SetMarkerStyle(20)
        gr.SetMarkerSize(1.0)
        # Check is there
        # The x-axis
        if which_x == 'elec':
            ytitle = 'Threshold [electrons]'
        elif which_x == 'vcal':
            ytitle = 'Threshold [#DeltaV_{cal}]'
        else:
            raise RuntimeError('Invalid x-axis name: "{}"'.format(which_x))
        # Let's start the loop over the TDACs
        ymin = 1e10 
        ymax = -1e10
        c1 = ROOT.TCanvas('c1','Thresholds')
        c1.Divide(4,4)
        hs = []
        for tdac in range(16):
            # Get the list of thresholds for this TDAC
            vthr = thrs_tdac[0][(thrs_tdac[1][:,:] == tdac) & (thrs_tdac[0][:,:] != 0)].flatten()
            xmin,xmax = np.min(vthr>0)*0.9,np.max(vthr)*1.1
            hs.append( ROOT.TH1F('h_{}_{}'.format(tdac,which_x),'TDAC={};Threshold [{}];# Pixels'.format(tdac,which_x),\
                    100,xmin,xmax) )
            # univoc name
            hs[-1].SetName('{}_{}'.format(hs[-1].GetName(),hs[-1]))
            hs[-1].SetMarkerStyle(20)
            hs[-1].SetMarkerColor(1)
            hs[-1].SetLineColor(1)
            # place it at the relevant pad
            pad=c1.cd(tdac+1)
            pad.SetTopMargin(0.1)
            pad.SetBottomMargin(0.12)
            pad.SetRightMargin(0.1)
            pad.SetLeftMargin(0.14)
            # and fill the histogram to fit afterwards
            for vthr_i in vthr:
                _ = hs[-1].Fill(vthr_i)
            # Just rebin if not enough statistics
            while float(hs[-1].GetEntries())/float(hs[-1].GetNbinsX()) < 10:
                hs[-1].Rebin(2)
            # Fit and extract the mean and std (we trust everything went well...)
            # Skewness or not (not zero allowed)
            # Only in the TDAC 0 and TDAC 15 could be a skew distribution, as they are
            # the extreme values which could found pixels unable to be accomodated 
            # around the mean value. 
            if tdac in [0,15]:
                # Skew normal distribution: https://en.wikipedia.org/wiki/Skew_normal_distribution
                gaus = ROOT.TF1('g','2/[2]*gaus(x)*0.5*(1.+TMath::Erf([3]*((x-[1])/(sqrt(2)*[2]))))',xmin,xmax)
                # Some initial estimations
                gaus.SetParameter(0,hs[-1].GetEntries())
                gaus.SetParameter(1,hs[-1].GetMean())
                gaus.SetParameter(2,hs[-1].GetStdDev()*2.0)
                # Remember shape parameter is defined [-1,1], the skew is related with this
                # shape in a complicated way
                # XXX - It is very complicated to converge both types of sensors,
                # This value and the Parameter-2 should be changed in order to properly 
                # converge, depending the sensor
                gaus.SetParameter(3,(-1)**(tdac+1)*15.0)
                # Obtain the quantiles in order to evaluate the asymmetric uncertainties
                # Note the skew distributions we can use the obtained sigma to evaluate
                # the uncertainty of the skew part, the other part has a smallest uncertainty
                # evaluated by incorporating the remaining population up to 98% of the total,
                # the difference w.r.t. the mean is quoted as uncertainty in there
                quantiles = np.array([0.98,1-0.98])
                x_q = np.array([0.,0.0])
                _=hs[-1].GetQuantiles(2,x_q,quantiles)
            else:
                gaus = ROOT.TF1('g','gaus',xmin,xmax)
            fst = -1
            _i = 0
            while fst != 0 and _i < 5:
                st = hs[-1].Fit(gaus,'QSR')
                fst = st.Status()
                _i += 1
            mean=gaus.GetParameter(1)
            std_low=std_high=gaus.GetParameter(2)
            if tdac == 0:
                std_high = x_q[0]-mean
            elif tdac == 15:
                std_low = mean-x_q[1]
            # Just in case the gaussian is skew, giving the shape parameter
            # (negative shape skew on the left, positive shape skew of the right)
            skewness=''
            if gaus.GetNumberFreeParameters() == 4:
                skewness = "Shape:{}".format(gaus.GetParameter(3))
            # Compare with the mean and std 
            print("[TDAC: {}] Fit status: {} ==>  Gaus fit mean:{} - Mean(list):{} | "\
                    "Gaus fit std:{} - Std(list):{} {}".format(tdac,st.Status(),mean,np.average(vthr),std_low,np.std(vthr),skewness))
            gr.SetPoint(tdac,tdac,mean)
            gr.SetPointEYlow(tdac,std_low)
            gr.SetPointEYhigh(tdac,std_high)
            if ymin > mean-std_low:
                ymin = mean-std_low
            if ymax < mean+std_high:
                ymax = mean+std_high
            # and plot (center the plot)
            hs[-1].GetXaxis().SetRangeUser(mean-5.0*std_low,mean+5.0*std_high)
            hs[-1].Draw('PE1')
        c1.SaveAs('{}_threshold_per_tdacs_fit_{}_{}.{}'.format(self._sensor,which_x,method,fmt))

        # Prepare the plot. Fixed Frame
        c0 = ROOT.TCanvas()
        h = c0.DrawFrame(-0.6,ymin*0.8,15.4,ymax*1.2)
        # Remember histo doesn't exist unil graph is plotted
        # XXX -- Looks like doesn't work
        h.SetTitle(';TDAC;{}'.format(ytitle))
        gr.Draw("PE1")
        
        c0.SaveAs('{}_tdac_vs_threshold_{}_{}.{}'.format(self._sensor,which_x,method,fmt))
    
        # Return same ROOT conditions
        ROOT.gROOT.SetBatch(batch)
        ROOT.gROOT.GetListOfCanvases().Delete()

        return gr

# XXX - Need split elec/vcal or just create it in teh same matrix
#       and therefore, used with the same class
class apply_calibration(object):
    """Class encapsulating the conversion from ToT to vcal or elec.
    It relies on the `tot_vcal_scan` class to create the calibration constants
    from a threshold scan file, after using the `dump_calibration_file` 
    method to create the calibration file to use.
    
    The file contains the constants used for the fit of the ToT-(vcal/elec)
    curves: 
        :math: f_{ToT}(x)= a x+b -\frac{c}{x-t}

    Therefore in order to be used, i.e., to convert ToT into vcal/elec:
        :math: f^{-1}_{ToT}(x) = \frac{at+x-b+\sqrt{(b+at-x)^2+4ac}}{2a}
    """
    def __init__(self,calfile,mask_large_slope=False):
        """
        Parameters
        ----------
        calfile: str
            Calibration file created by the `tot_vcal_scan` class. 
        mask_large_slope: bool
            Whether to remove (mask) pixels with a large fitted slope, 
            larger than 10kelectrons per ToT (make no sense and indicates
            the fit failed). 
            If this is false, instead of masking it will asign to the 
            nonsense pixel an average value 

        """
        # function behaviour dependent on whether the mask is applied or not
        self.is_pixel_masked = None

        pre_masked = np.load(calfile)
        if mask_large_slope:
            self.is_pixel_masked = self._is_pixel_masked
            # The parameters array
            self._ctes = np.ma.zeros(shape=pre_masked.shape)
            # Mask those pixels with non-sense slopes
            self._ctes[0] =  np.ma.array(pre_masked[0],mask=(pre_masked[0] < 1e-5))
            # Propagate mask to all parameters
            for i in xrange(1,len(pre_masked)):
                self._ctes[i] = np.ma.array(pre_masked[i],mask=self._ctes[0].mask)
        else:
            self.is_pixel_masked = self._is_always_valid
            # The parameters array
            self._ctes = np.ma.zeros(shape=pre_masked.shape)
            # Get those pixels with non-sense slopes
            slope_properval = np.ma.array(pre_masked[0],mask=((pre_masked[0] < 1e-5) & (pre_masked[0] > 0.0) ))
            # Propagate mask to all parameters
            for i in xrange(0,len(pre_masked)):
                cte_properval = np.ma.array(pre_masked[i],mask=slope_properval.mask)
                # Subtitute the masked values by the mean of the unmasked
                #self._ctes[i] = cte_properval.filled(cte_properval.mean())
                # Evaluate the median, instead of the mean
                self._ctes[i] = cte_properval.filled(np.ma.median(cte_properval))
        
        # Assumes standard name convention
        self._sensor = os.path.basename(calfile).split('_')[0]
        self._x      = os.path.basename(calfile).split('_')[5].split('.')[0]
        # Only use the 'vcal', the conversion will be done here
        if self._x != 'elec' and self._x != 'vcal':
            raise RuntimeError('Please use the standarized convention for the file name')
        if self._x == 'elec':
            print('Use the "vcal" version, the "ecal" will be deprecated soon.')
            # XXX -- raise RuntimeError

        # A memoization for not repeating calculations
        self._response_back = lambda ToT : (self.a*self.t + ToT - self.b + \
                np.sqrt((self.b+self.a*self.t-ToT)**2.0+4.0*self.a*self.c))/(2.0*self.a)

    @property
    def a(self):
        """The slope
        """
        return self._ctes[0]

    @property
    def b(self):
        """The offset
        """
        return self._ctes[1]

    @property
    def c(self):
        """The rising curvature
        """
        return self._ctes[2]

    @property
    def t(self):
        """The threshold
        """
        return self._ctes[3]

    def __call__(self,col,row,ToT):
        """The calibration response

        Parameters
        ----------
        col: int
            Pixel column
        row: int
            Pixel row 
        ToT: float
            The time over threshold measurement

        Return
        ------
        The measured charge in vcal/elec units
        """
        return self._response_back(ToT)[col,row]

    def _is_always_valid(self,col,row):
        """ Dummy function returning always False, in 
        order to be compatible with the `is_pixel_masked` 
        expected behaviour function
        """
        
        return False

    def _is_pixel_masked(self,col,row):
        """Whether a col,row is masked, therefore there is
        no calibration curve available
        
        Parameters
        ----------
        col: int
            Pixel column
        row: int
            Pixel row 

        Return
        ------
        bool: True if is a masked pixel

        Raise
        -----
        AttributeError if this is not a masked Array
        """
        # FIXME: assert? -- it's always a masked Array, right?
        return bool(self._ctes[0].mask[col,row])

    def dump_text_file(self,filename,a_cutoff=1e-5):
        """FIXME
        """
        # FIXME: TO BE REMOVE OR SEND to the tot_vcal class
        # Just keep those entry with sensible values (a slope lower than
        # 1/a= 10000 something/ToT (i.e. a = 1e-5) makes no sense..
        # evn lower will make no sense
        # FIXME: DOCUMENT cutoff (which is this thing
        ascii_lines = []
        for (col,row) in np.argwhere(np.abs(self.a) > a_cutoff):
            # col row a b c t
            ascii_lines.append('{} {} {} {} {} {}\n'.format(col,row,
                self.a[col,row],self.b[col,row],self.c[col,row],self.t[col,row]))

        with open(filename,'w') as f:
            f.writelines(ascii_lines)

class calibration_plotter(apply_calibration):
    """Class used to plot the calibration curves
    """
    def __init__(self,calcfile,rootfile=None):
        """
        Parameters
        ----------
        calfile: str
            Calibration file created by the `tot_vcal_scan` class. 
        rootfile: str
            The equivalent rootfile, if None extracts the root file
            name from calcfile, substituting `.npy` by `.root`
        """
        super(calibration_plotter,self).__init__(calcfile)
        # And the equivalent ROOT file
        if rootfile is not None:
            self._rootfile = ROOT.TFile.Open(rootfile)
        else:
            self._rootfile = ROOT.TFile.Open(calcfile.replace('_vcal.npy','.root').replace('_elec.npy','.root'))
    
    def plot_all_calibration_curves(self):
        """Plot the calibration curves and measured values for all pixels
        in an unique canvas (one per each)

        Return
        ------
        str,str: the name of the plot files (measurement, fitted)
        """
        style=pl.set_sifca_style(squared=True,stat_off=True)
        style.cd()
        ROOT.gROOT.SetBatch()

        # Extract all curve to obtain limits on the histos
        calcurves = []
        measurements = []
        xmin = 0
        ymin = 0 
        ymax = 14
        # Some 
        nkeys = len(self._rootfile.GetListOfKeys())
        with Bar("Extracting pixels calibration curves",max=nkeys,suffix='%(percent)d%%') as bar:
            for key in filter(lambda key: key.GetClassName().find('TGraph') == 0,self._rootfile.GetListOfKeys()):
                gname=key.GetName()
                # The measurements (means of each injection step)
                mgr = self._rootfile.Get(gname)
                # The calibration curve
                try:
                    calcurves.append(mgr.GetListOfFunctions()[0])
                except IndexError:
                    # Calibration curve not present, fill it with None
                    calcurves.append(None)
                # Fill the array of measurements points already
                n = mgr.GetN()
                ary = np.zeros((n,2))
                try:
                    ary[:,0]=mgr.GetX()
                    ary[:,1]=mgr.GetY()
                except ReferenceError:
                    # Covering the case no point was actually found in the graph...
                    # Not needed in python3
                    pass
                # Be sure to discard those dummy values in the graph with no measured values)
                measurements.append(ary[ary[:,0]>1e-1])
                bar.next()
        # Found limits for the histos
        xmin = measurements[0].min()*0.97
        xmax = measurements[1].max()*1.03
        # Binning like the number of points measured (a 40% more)
        nxbins = int(len(measurements[0])*1.4)
        title = "#DeltaV_{cal}"
        hmeas = ROOT.TH2F('meas',';{};Measured Charge [ToT];# pixels'.format(title),\
                nxbins,xmin,xmax,100,ymin,ymax)
        
        # For the curves, lets create a more dense array and doubling the range 
        # Fixing xmax to 1800 DVcal
        xmax = 1800
        xcurves = np.linspace(xmin,xmax,num=100)
        dx = xcurves[1]-xcurves[0]
        hcurv = ROOT.TH2F('tot',';{};Measured Charge [ToT];# pixels'.format(title),\
                len(xcurves),xcurves[0]-dx*0.5,xcurves[-1]+dx*0.5,100,ymin,ymax)
        # Fill the histos: for all pixels
        with Bar("Populating histograms               ",
                max=len(calcurves),suffix='%(percent)d%%') as bar:
            for ipx,cc in enumerate(calcurves):
                xyarr = measurements[ipx]
                for i,(x,y) in enumerate(xyarr):
                    _ = hmeas.Fill(x,y)
                # Not present
                if cc is None:
                    bar.next()
                    continue
                # Create more points, at least 2xtims range
                current_threshold = cc.GetParameter(3)
                # Be careful not to add any point before the asymptot (at threshold)
                for xc in xcurves[xcurves>current_threshold]:
                    _ = hcurv.Fill(xc,cc(xc))
                bar.next()
        c = ROOT.TCanvas()
        c.SetLogz()
        # Prepare the electron axis
        xmin,xmax = hmeas.GetXaxis().GetBinLowEdge(1),hmeas.GetXaxis().GetBinLowEdge(hmeas.GetNbinsX()+1)
        ymin = hmeas.GetYaxis().GetBinLowEdge(1)
        elec_axis = prepare_electron_axis((xmin,xmax),ymin,hmeas,style)
        hmeas.Draw('COLZ')
        elec_axis.Draw()
        mfname = '{}_calibration_measurements_curve_allpixels.png'.format(self._sensor)
        c.SaveAs(mfname)
        # Same for hcurv
        hcurv.Draw('COLZ')
        xmin,xmax = hcurv.GetXaxis().GetBinLowEdge(1),hcurv.GetXaxis().GetBinLowEdge(hcurv.GetNbinsX()+1)
        ymin = hcurv.GetYaxis().GetBinLowEdge(1)
        elec_axis = prepare_electron_axis((xmin,xmax),ymin,hcurv,style)
        #hcurv.Draw('COLZ')
        elec_axis.Draw()
        cfname='{}_calibration_curves_curve_allpixels.png'.format(self._sensor)
        c.SaveAs(cfname)

        return mfname,cfname

    def plot_distributions(self):
        """Plot the distribution of the calibration constant 
        for all pixels
        """
        style=pl.set_sifca_style(squared=True,stat_off=True)
        style.cd()
        style.SetOptStat(111111)
        ROOT.gROOT.SetBatch()

        # XXX I need a enable pixel mask to skip not-enable pixel!!

        # The axis limits depending the unit
        if self._x == 'vcal':
            xs = 0,300
            xc = 20,100
            xt = 40,300
            xunit='#DeltaV_{cal}'
        elif self._x == 'elec':
            xs = 0,3000
            xc = 20,100
            xt = 400,3000
            xunit = 'Electrons'

        
        hslope = ROOT.TH1F('slope',';(inverse) linear slope [{}/ToT];# pixels'.format(xunit),200,xs[0],xs[1])
        hoffset= ROOT.TH1F('offset',';offset [ToT];# pixels',200,-4,4)
        hcurvature= ROOT.TH1F('curvature',';c [ToT {}];# pixels'.format(xunit),200,xc[0],xc[1])
        hthreshold= ROOT.TH1F('threshold',';threshold [{}];# pixels'.format(xunit),100,xt[0],xt[1])
        # Keep the same order than the parameters in the function
        ghistos = [hslope,hoffset,hcurvature,hthreshold]
        functs  = [lambda x: x**-1,lambda x: x,lambda x: x,lambda x: x]
        # Change loop behavior depending masked or not
        if type(self._ctes[0]) == np.ma.MaskedArray:
            looping = lambda the_array: the_array.compressed()
        else:
            looping = lambda the_array: the_array
        for i,h in enumerate(ghistos):
            for col_array in self._ctes[i]:
                for val in looping(col_array):
                    # XXX --- PATCH!!!
                    if val == 0:
                        continue
                    # XXX --- PATCH!!!
                    try:
                        final_val = functs[i](val)
                    except ZeroDivisionError:
                        final_val = 0.0
                    # Ignore 0-values from masked pixels
                    h.Fill(final_val)
            c = ROOT.TCanvas()
            h.Draw()
            c.SaveAs('{}_calibration_ctes_allpixels_{}_{}.png'.format(self._sensor,h.GetName(),self._x))
            c.SetLogy()
            h.Draw()
            c.SaveAs('{}_calibration_ctes_allpixels_{}_{}_log.png'.format(self._sensor,h.GetName(),self._x))

def prepare_electron_axis((xmin,xmax),ymin,frame,style,
        title="Electrons          ",conversion=QDAC_Conversor(VREF)):
    """Prepare all elements to create the electron axis along the DVcal one

    Parameters
    ----------
    (xmin,xmax): (float,float)
        The range in the Vcal original units
    ymin: float
        The y-position where to place the axis 
    frame: ROOT.TH1
        The frame
    style: ROOT.TStyle

    Returns
    -------
    ROOT.TGaxis
    """
    # Move 
    frame.SetTitleOffset(1.5)
    elec_axis = ROOT.TGaxis(xmin,ymin,xmax,ymin,conversion(xmin),conversion(xmax),405,"-")
    elec_axis.SetNoExponent(True)
    #color_axis=ROOT.kTeal+3
    color_axis=ROOT.kGray+2
    elec_axis.SetLabelColor(color_axis)
    elec_axis.SetTitleColor(color_axis)
    elec_axis.SetLineColor(color_axis)
    elec_axis.SetLabelFont(style.GetLabelFont())
    elec_axis.SetLabelSize(style.GetLabelSize())
    elec_axis.SetTitleFont(style.GetTitleFont())
    elec_axis.SetTitleSize(style.GetTitleSize())
    elec_axis.SetLabelOffset(-0.08)
    elec_axis.SetTitle(title)
    elec_axis.SetTitleOffset(-1.45)

    return elec_axis

# XXX -  FIXME -- The same code is present in the class, probably
# this could be improved and re-used
def plot_calibration(fname,sensor,col,row,which_x='elec',fmt='png'):
    """Plot the calibration curve for the pixel

    Parameters
    ----------
    fname: str
        The input root file name where the calibration curves are stored
    sensor: str
        The name of the sensor, to be used in the plot file name
    col: int
        Pixel column
    row: int
        Pixel row
    which_x: str
        What x-axis used, either `elec` or `vcal` [Default: elec]
    fmt: str
        The plot suffix [Default: png]
    ghistos: list(ROOT.TH1F)|None
        If present the global histograms for the parameters. 
        Note, the a (slope) and b (offset) parameters are 
        expected to store the inverse of them            
    """
    raise RuntimeError("NOT WORKING YET!!")
    style=pl.set_sifca_style(squared=True,stat_off=True)
    style.cd()
    batch=ROOT.gROOT.IsBatch()
    ROOT.gROOT.SetBatch()
    # Open the file
    f = ROOT.TFile.Open(fname)
    # Get the curve from the file
    gr = f.Get('cal_curve_{}_{}_{}'.format(which_x,col,row))
    gr.UseCurrentStyle()
    gr.SetMarkerStyle(20)
    gr.SetMarkerSize(1.0)
    # Check is there
    if not isinstance(gr,ROOT.TGraphErrors):
        print("No calibration curve present for pixel {}-{}".format(col,row))
        return
    
    # The x-axis
    if which_x == 'elec':
        xtitle = 'electrons'
        xmin = 500
    elif which_x == 'vcal':
        xtitle = '#DeltaV_{cal}'
        xmin = 0
    else:
        raise RuntimeError('Invalid x-axis name: "{}"'.format(which_x))
    # Prepare the plot. Fixed Frame
    c0 = ROOT.TCanvas()
    _ = gr.Draw('APE')
    # Remember histo doesn't exist unil graph is plotted
    h = gr.GetHistogram()
    h.GetYaxis().SetRangeUser(0,h.GetMaximum())
    # XXX -- Looks like doesn't work
    h.GetXaxis().SetRangeUser(xmin,h.GetXaxis().GetBinLowEdge(2))
    h.SetTitle(';{};<ToT>'.format(xtitle))
    # Plot all elements
    h.Draw()

    # Get The fit function
    pf = gr.GetFunction('tot_{}_{}_{}'.format(which_x,col,row))
    if not isinstance(pf,ROOT.TF1):
        print("No fitted function for curve of pixel {}-{}".format(col,row))
        return

    slope = pf.GetParameter(0),pf.GetParError(0)
    inv_slope = (slope[0]**-1,slope[0]**-2*slope[1])
    offset = pf.GetParameter(1),pf.GetParError(1)
    try:
        inv_offset = (offset[0]**-1,offset[0]**-2*offset[1])
    except ZeroDivisionError:
        inv_offset = (0.0,0.0)

    threshold = pf.GetParameter(3),pf.GetParError(3)
    #print(threshold[1])
    # Print the fit output
    ypos= 0.83
    xpos=0.45
    lp1=ROOT.TLatex()
    lp1.DrawLatexNDC(0.1,0.92,'Pixel: {}_{} {}_{}'.format(col,'{col}',row,'{row}'))
    lp1.DrawLatexNDC(xpos,ypos,'1/a={:.0f}#pm{:.0f} [{}/ToT]'.format(inv_slope[0],inv_slope[1],which_x))
    lp1.DrawLatexNDC(xpos,ypos-0.05,'1/b={:0.1f}#pm{:0.1f} [{}]'.format(inv_offset[0],inv_offset[1],which_x))
    lp1.DrawLatexNDC(xpos,ypos-0.1,'t_{}={:.0f}#pm{:.0f} [{}]'.format('{thr}',threshold[0],threshold[1],which_x))
    lp1.DrawLatexNDC(xpos-0.3,ypos,'ax + b - #frac{}{}'.format('{c}','{x-t_{trh}}'))

    # Threshold and uncertainty
    thr_err = ROOT.TBox(threshold[0]-threshold[1],0,threshold[0]+threshold[1],h.GetMaximum())
    #thr_err.SetFillColor(ROOT.kGray)
    thr_err.SetFillColorAlpha(ROOT.kGray,0.5)
    thr_err.Draw('F')

    thr = ROOT.TLine(threshold[0],0,threshold[0],h.GetMaximum())
    thr.SetLineWidth(2)
    thr.SetLineColor(ROOT.kRed+3)
    thr.Draw()
    # Also a label
    lp1.DrawLatex((threshold[0]+threshold[1])*1.05,h.GetMaximum()*0.95,'t_{thr}')

    # And the curve
    gr.Draw('PE1')

    c0.SaveAs('{}_calibration_tot_{}_pixel_{}_{}.{}'.format(sensor,which_x,col,row,fmt))
    
    # Return same ROOT conditions
    ROOT.gROOT.SetBatch(batch)
    ROOT.gROOT.GetListOfCanvases().Delete()


def calibration_process(ifile,only_file):
    """Create the calibration constans file

    Parameters
    ---------
    ifile: str
        The h5 file obtained from a threshold scan
    only_file: bool
        Just create the calibration files, no plots in output        
    """
    # Get the file threshold scan
    a = tot_vcal_scan(ifile)
    # Set Minuit2 as minimizer (better and faster)
    #ROOT.Math.MinimizerOptions.SetDefaultMinimizer("Minuit2")
    ROOT.Math.MinimizerOptions.SetDefaultPrintLevel(0)

    filename = a.dump_calibration_file()
    print("Created calibration file for {}: {}".format(a._sensor,filename))
    a.store_calibration_curves()
