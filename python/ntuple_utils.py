#!/usr/bin/env python
"""Accessors utils to the ntuple created by 
the EUTelescope processor: EUTelAPIXTbTrackTuple

XXX DOC
XXX Check of invariants

jorge.duarte.campderros@cern.ch CERN/IFCA 2018.11.13
"""
__author__ = "Jordi Duarte-Campderros"
__credits__ = ["Jordi Duarte-Campderros"]
__version__ = "0.1"
__maintainer__ = "Jordi Duarte-Campderros"
__email__ = "jorge.duarte.campderros@cern.ch"
__status__ = "Development"

import time
import numpy as np
from scipy.ndimage import measurements as cluster_finder

import ROOT

from sensor_description import sensor_layout

from .calibrate import apply_calibration
from .cluster_reprocessor import Clusters

REF_ID_RANGES = (20, 29)

def extract_metadata(metadata,process_dut):
    """Extracts and organises the metadata of the TTree

    Parameters
    ----------
    metadata: TTree
        The metadata tree
    process_dut: int
        The ID number for the DUT to process

    Returns
    -------
    (int,(int,int,str): metatdata info
        (The reference plane order, (the dut plane order, dut ID, the dut layout))
    """
    # -- Find the the REF plane (DUT-ID in [20,29]
    _=metadata.GetEntry(0)
    # The planes ID
    planes_id_str = str(metadata.PlanesId)
    planes_id = map(lambda x: int(x),planes_id_str.split(':'))
    ## And the Z-oreder index
    ### WTF --- planes_zorder_str = str(metadata.PlanesZorderId)
    ### planes_zorder = map(lambda x: int(x),planes_id_str.split(':'))
    try:
        ref_plane_id = filter(lambda (i,x): REF_ID_RANGES[0] <= x < REF_ID_RANGES[1], enumerate(planes_id))[0]
    except IndexError:
        raise RuntimeError("Unconsistent metadata in, no reference sensor, or"\
            " outside expected ID ranges '{0}'".format(REF_ID_RANGES))
    ref_id    = ref_plane_id[1]
    ref_plane = ref_plane_id[0]
    # Find the DUT and fill all relevant info
    try:
        dut_plane_id = filter(lambda (i,x): x == int(process_dut), enumerate(planes_id))[0]
    except IndexError:
        raise RuntimeError("DUT with ID: {0} is not present".format(process_dut))
    dut_id    = dut_plane_id[1]
    dut_plane = dut_plane_id[0]
    # And the layout of the dut
    the_layouts = str(metadata.DUTLayouts)
    dut_layout = the_layouts.split(':')[dut_plane]
    # Just to avoid me to change things over there
    if dut_layout == "100x25":
        dut_layout = "25x100"

    return ref_plane,(dut_plane,dut_id,dut_layout)

def pythonize(classname):
    """XXX DOC
    """
    if classname == "":
        return  np.array([-1],dtype=np.int32)
    return eval(classname.replace("vector<","ROOT.vector(\"").\
            replace(">","\")()"))

# -- Helper functions
def class_decorator(tfile,treename):
    """Creates the properties of the relevant classes (trees of 
    the ntuple) inheriting from the TBTree class. 

    Parameters
    ----------
    tfile: ROOT.TFile
        The ntuple containing all the relevant ttrees
    treename: str
        The tree to be decorateed
    """
    cls=TREENAME_TBTree[treename]
    for branch in tfile.Get(treename).GetListOfBranches():
        make_property(cls,branch.GetName())
    
class tree_gatherer(object):
    """Class to drive common actions/methods to be
    done synchronously to all trees on the ntuple.
    The class initilizes empty, and after the TBTrees
    has been built, set them as attributes
    """
    # XXX --- The order is important, dont't change it unless 
    # XXX --- you know what your do it
    # XXX --- THIS IS CRITICAL WHEN REPROCESSING THE CLUSTERS!!
    # FIXME - CREATE A TEST!
    _ntuple_elements = [ 'tracks', 'fitpoints', 'rawdata', 'meashits' ]

    def __init__(self):
        pass

    def get_entry_all(self,i):
        for treename in self._ntuple_elements:
            _ = getattr(self,treename)._store.GetEntry(i)

    def set_dut_metadata(self,dutdict,check_all_init=True):
        """Asssume a meashits class instance is present

        Parameters
        ----------
        ductdict: dict(int,(str,int))
            The dut sensor layout and plane order (0-closest to the
            beam entrance) in the telecope setup
        check_all_init: bool
            Whether or not check that all DUT presents in the TFile
            are set
        """
        # Cross-check we have the dut in there
        ninit = 0
        dut_registered = []
        for did,(layout,plane) in dutdict.iteritems():
            try: 
                self.meashits.xpitch[did],self.meashits.ypitch[did] = sensor_layout(layout).xpitch,sensor_layout(layout).ypitch
                self.meashits.xsize[did],self.meashits.ysize[did]   = sensor_layout.XSIZE,sensor_layout.YSIZE
                self.meashits.dutplane[did] = plane
                dut_registered.append(did)
            except KeyError:
                raise RuntimeError("DUT '{0}' not present in the file".format(key))
            ninit+=1
        
        if not check_all_init:
            # Using only 1-dut, be sure is marked here,
            # XXX Also, a weak invariant is assumed here... TBC
            # FIXME ---- NOT WORKNING WITH TTREE!!!
            self.meashits._store._use_only_dut = dut_registered[0]

        if check_all_init and ninit != len(self.meashits.xpitch):
            message = "Not initialized layout for DUT(s):"
            message += " {0}".format(filter(lambda did: did not in dutdict.keys(),self.meashits.xpitch.keys()))
            raise RuntimeError(message)


def prepare_tree(tfile,tree_name,**args):
    """ Create the TBTree concrete class using the `tbtree_builder`
    function 

    tree_name: str
        The TTree name
    args: optional arguments needed for certain TBTree concrete classes
    """
    # -- Prepare each class to be used (with the appropieate
    #    properties and data members
    class_decorator(tfile,tree_name)

    tree=tfile.Get(tree_name)
    # Create all the needed classes
    return tree,tbtree_builder(tree,**args)

def prepare_all_trees(tfile,args_dicts={}):
    """Get all relevant trees in the ntuple and return them 
    in a compact dummy container

    Parameters
    ----------
    tfile: ROOT.TFile
        The Ntuple created by the EUTelescope processor: EUTelAPIXTbTrackTuple
    args_dicts: dict|None
        The dictionary to pass the arguments to each tree. The dictionary
        must follow the structure:
        { 'treename': { 'arg': value , ... }, ... }

        Valid keys dictionaries and keys:
            * fitpoints : { 'drift_x': (float,float), 'drift_y': (float,float) }
            * meashits: { 'recalculate_clusters': bool }

    Return
    ------
    TODO

    Example
    -------
    TODO
    """
    _a=tree_gatherer()

    for element_name in _a._ntuple_elements:
        treename = '{}_tree'.format(element_name)
        extra_args = {}
        if args_dicts.has_key(element_name):
            extra_args = args_dicts[element_name]
        tree,element=prepare_tree(tfile,element_name,**extra_args)
        setattr(_a,treename,tree)
        setattr(_a,element_name,element)
    # Now set to the relevant classes the xpitch/ypitch of each dut

    # XXX PROVISONAL: Create a pseudo Close function in order to delete the `fitpoints_tree`
    #                 which is causing a segmentatio violation (double deallocation?)   
    tfile._the_old_close = tfile.Close
    def _tfile_closer():
        # Not working, it seems race condition?
        _a.fitpoints_tree.Delete()
        # Adding the sleep, it will work...
        time.sleep(1)
        tfile._the_old_close()
    tfile.Close = _tfile_closer

    return _a

def make_property(cls,name_property):
    """Create dynamically the setters and getters to create properties
    to the TBTree class. The property is defined to be one of the 
    branches of the tree. 

    Parameters
    ----------
    cls: The class to be extended with the attributes
    name_property: str
        The name of the property
    """
    datamember = "_"+name_property
    def the_getter(self):
        return getattr(self,datamember)
    
    def the_setter(self,tree):
        if not isinstance(tree,ROOT.TTree):
            raise RuntimeError("Read-only object: measured hits stored in a TFile")
        # Initialze the attribute with the pythonic container of the branch
        setattr(self,datamember,pythonize(tree.GetBranch(name_property).GetClassName()))
        # And assign it to the Branch
        tree.SetBranchAddress(name_property,getattr(self,datamember))

    # Use the branches as properties never initialize except in construction 
    # with the Branch address
    setattr(cls,name_property,property(the_getter,the_setter))
    setattr(cls,"is_decorated",True)

def override_property(cls,datamember):
    """Special function to avoid the use of the Tree branch, by
    overriding the full Tree. Use when re-processing is needed.

    The overriding is performed by getting directly the 
    `datamember` of the `_store`, instead of `_datamember` of `self`.
    Therefore, `_store` should be pointing to a different class instance
    then the TTree (Clusters, for instance). If `_store` is pointing
    to the TTree, this function is doing nothing (as intended).

    Parameters
    ----------
    cls: The class to be change the attribute getter
    datamember: str
        The name of the property
    """
    def the_getter(self):
        return getattr(self._store,datamember)
    setattr(cls,datamember,property(the_getter))


class TBTree(object):
    """Abstract class to encapsulate all the ttrees from
    the EUTelAPIXTbTrackTuple ntuple
    """
    def __init__(self,tree,**args):
        """
        tree: ROOT.TTree
            The tree object
        args: Optional arguments (see the concrete classes)
        """
        if not hasattr(self,"is_decorated"):
            raise RuntimeError("The method 'class_decorator' must be called before"\
                    " in order to populate the attributes of this class (branches of the ttree)")
        if tree.GetName() != self._needed_treename:
            raise RuntimeError("Incompatible class to use with the '{0}' TTree".format(tree.GetName()))
        self._store = tree
        for brname in map(lambda x: x.GetName(),tree.GetListOfBranches()):
            setattr(self,brname,self._store)

class hits(TBTree):
    """Measured hits in the DUT after clusterization, i.e. sensor 
    estimation of a particle passing point
    """
    def __init__(self,tree,recalculate_clusters=False):
        """
        Parameters
        ----------
        tree: ROOT.TTree
            The measurement hits tree
        recalculate_clusters: bool
            Whether or not re-do the cluster analysis. If not, the ' meashits' 
            branch is used from the input 'tfile' . If the clusters are asked
            to be recalculated, the `rawdata' tree is going to be used to create
            clusters from there. Also `fitpoints` and `tracks` trees are going 
            to be used (and maybe modify slight some methods) to match clusters
            with tracks.
        """
        self._needed_treename="meashits"
        super(hits,self).__init__(tree)
        
        # Get the available DUTs id
        i = 0
        while True:
            dummy=tree.GetEntry(i)
            if len(self.sensorId) == 0:
                i+=1
                continue
            self.xpitch = dict(map(lambda i: (i,None),self.sensorId))
            self.ypitch = self.xpitch.copy()
            self.xsize  = self.xpitch.copy()
            self.ysize  = self.xpitch.copy()
            # The plane number within the test beam setup
            self.dutplane = self.xpitch.copy()
            break
        tree.GetEntry(0)

        # Helper member for the cluster reprocessing case
        self._use_only_dut = None

        self._cluster_reprocessing = recalculate_clusters
        if self._cluster_reprocessing:
            # If the user wants to recalculate the clusters, we need
            # to create a new pointers to the recalculator
            branches = [ br.GetName() for br in self._store.GetListOfBranches() ]
            self._store = Clusters()
            # Populate pixel layout datamembers
            self._store.xpitch = self.xpitch
            self._store.ypitch = self.ypitch
            self._store.xsize = self.xsize
            self._store.ysize = self.ysize
            self._store.dutplane = self.dutplane
            self._store._use_only_dut = self._use_only_dut
            for attr in dir(self):
                if attr in branches:
                    # Overwritte the property definitions for the branches,
                    # instead make use of the Clusters 
                    override_property(hits,attr)

    def get_pixels_closest_distance(self,ihit,pxls,pxid_list):
        """Get the closests pixels to a hit. The number of 
        returned pixel indices is equal to the number of clusterSize.
        Note that the condition to be associated is either row or column are 
        consecutives

        Returns
        -------
        list(int): The list of pixels indices forming this cluster
        """
        dut = self.sensorId[ihit]

        positions=pxls.get_positions(pxid_list)
        # Translate the hit position to colums, remember to remove
        # the extra 0.5 which was added in the hit creation 
        hitpos =np.array((self.xPos[ihit]/self.xpitch[dut],self.yPos[ihit]/self.ypitch[dut]),'f')
        # Order by distance, keep only the closest pixels compatible with clusterSize
        # -- Calculate the distances
        # --> hitpos is broadcasted (https://www.scipy-lectures.org/intro/numpy/operations.html#broadcasting)
        # -- Obtain distance between coordinates
        predist = (hitpos-positions)**2.0
        # -- add-up coordinates, array flatenned to 1. The index corresponds
        #    to the pixel index: pxid_lita
        try: 
            # Just extract enough close points
            distance = np.sqrt(predist[:,0]+predist[:,1])
        except IndexError:
            # The position array only contains 1 point
            distance = np.sqrt(predist[0]+predist[1])
        # Sorting by minimum distance
        closest_pixels_local_id= np.argsort(distance)
        # Correcting the element id from distance, therefore pxid_list
        # by the CONTENT of pxid_list
        closest_pixels = np.array(pxid_list)[closest_pixels_local_id]
        # Get the pixels clustered in order to assign the group of pixels
        # along with the closest pixel
        clusters = pxls.get_connected(pxid_list)
        # find the group of pixels
        closest_cluster = filter(lambda plist: closest_pixels[0] in plist,clusters)[0]
        assert self.clusterSize[ihit] == len(closest_cluster)
        
        # keep the first clusterSize-elements
        return closest_cluster
        
 
# Helper class for pixels
# A auto-generated dictionary, if the key 
# doesn't exist it returns the dummy function
class auto_lambda_dict(dict):
    def __missing__(self, key):
        self[key] = lambda col,row,tot: tot
        return self[key]

class pixels(TBTree):
    """Zero supressed raw data, i.e pixels with charge
    from the DUT.
    """
    def __init__(self,tree):
        """
        """
        self._needed_treename="rawdata"
        super(pixels,self).__init__(tree)

        # Just dummy function if there is no calibration
        self._tot_to_phys = auto_lambda_dict()
        self.calibration_curves_present = {}

    def set_calibrations(self,dutid,calfile):
        """Set the calibration curves

        Parameters
        ----------
        dutid: int
            The DUT index
        calfile: str
            The calibration file, previously created with XXX

        See Also
        --------
        `apply_calibration`
        """
        print("Calibration file for DUT-{}: {}".format(dutid,calfile))
        self.calibration_curves_present[dutid] = True
        self._tot_to_phys[dutid] = apply_calibration(calfile)

    def valid_calibration_pixel(self,dutid,col,row):
        """Whether or not the calibration curve is present

        Parameters
        ----------
        col: int
            Pixel column
        row: int
            Pixel row 

        Return
        ------
        bool: True if the pixel has a calibration curve
        """
        # -- if there was no calibration, just accept everything
        try: 
            if not self.calibration_curves_present[dutid]:
                return True
        except KeyError:
            return True

        return (not self._tot_to_phys[dutid].is_pixel_masked(col,row))
    
    def get_charge(self,idx):
        """Get the charge of a given pixel, if calibration is present,
        the calibration is applied providing physical values, otherwise
        ToT units are returned

        Parameters
        ----------
        idx: int
            The index of the current pixel in the event

        Returns
        -------
        float: (calibrated) charge
        """
        return self._tot_to_phys[self.iden[idx]](self.col[idx],self.row[idx],self.tot[idx])

    def get_indices_per_dut(self,only_dut=None):
        """Extract the indices of pixels corresponding of each DUT
        present in the event

        Paramaters
        ----------
        only_dut: int|None
            If not none, just the indices related with the dut are returned
        
        Returns
        -------
        dict(np.array(int)): a list of pixel indices corresponding to each
            DUT (key of the dictionary)
        """
        sid = np.array(self.iden.data())
        if only_dut is not None:
            duts = [only_dut]
        else:
            duts = list(set(sid))
        indxs = {} 
        for dut in duts:
            indxs[dut] = np.where(sid == dut)[0]
            # -- XXX Why it cannot be use this?? Dont' want to bias
            #        clusters by not using uncalibrated pixels
            # Warnings when comparing masked indices if not removed
            #prov_indsx = np.where(sid == dut)[0]
            #indxs[dut] = []
            #for idx in prov_indsx:
            #   if not self._tot_to_phys[dut].is_pixel_masked(self.col[idx],self.row[idx]):
            #        indxs[dut].append(idx)

        return indxs
    
    def get_positions(self,pxid_list):
        """Obtain the pixel position of pixels in col and rows

        Parameters
        ----------
        pxid_list: np.array(int) 
            The list of pixel indices to get the position

        Return
        ------
        positions: np.array([(col,row)])
            The position in pixel coordinate
        """
        # Look for differnt columns
        return np.array(zip(np.array(self.col.data())[pxid_list],np.array(self.row.data())[pxid_list]),dtype=np.long)
    
    def get_coordinate_size(self,pxid_list):
        """Obtain the number of pixels with different rows and columns

        Parameters
        ----------
        pxid_list: list(int)
            The list of pixel to check

        Return
        ------
        cluster_size: (int,int)
            The number of different columns (X) and rows(X)
        """
        pos=self.get_positions(pxid_list)
        return (np.unique(pos[:,0]).shape[0],np.unique(pos[:,1]).shape[0])

    def get_connected(self,pxid_list):
        """Obtain collection of connected elements from the input list

        Parameters
        ----------
        pxid_list: list(int)
        """
        # get columns and rows
        pos = self.get_positions(pxid_list)
        # Create the image input to the clustering finding function
        im=np.zeros(shape=(np.max(pos[:,0])+1,np.max(pos[:,1])+1))
        # with pixels actives in the columns and rows 
        im[pos[:,0],pos[:,1]]=1
        # allowing connections with up, down and diagonal directions
        lw,num=cluster_finder.label(im,structure=[[1,1,1],[1,1,1],[1,1,1]])
        # creates the different set for each cluster found
        clusters_list = []
        for iset in xrange(num):
            # obtain the index of the local pxid_list
            local_id=np.argwhere(lw[pos[:,0],pos[:,1]] == iset+1).flatten()
            # extract the actual pixel index and append the list
            clusters_list.append(np.array(pxid_list)[local_id])
        return clusters_list

    def get_total_charge(self,pixel_list):
        """Obtain the sum of the total charge for all the 
        pixels in the list

        Parameters
        ----------
        pixel_list: list(int)
            The pixel indices list to add up

        Returns
        -------
        total charge
        """
        return sum(map(lambda i: self.get_charge(i),pixel_list))
    
    def get_ordered_tot_pixels(self,pixel_list):
        """Obtain the pixel with highest ToT en X and in Y

        Parameters
        ----------
        pixel_list: list(int)
            The pixel indices list to add up

        Returns
        -------
        list: ToT wise ordered pixel indices
        """
        # Array of tot
        # XXX -- Do it with a numpy??
        tot_pxls = np.array(map(lambda i: self.tot[i],pixel_list),'i')
        # sort along (the array) columns
        return list(np.argsort(tot_pxls))


class fitpoints(TBTree):
    """Predicted points at the DUT, the best estimator of a particle
    """
    def __init__(self,tree,drift_x=None,drift_y=None):
        """
        tree: ROOT.TTree
        drift_x: (float,float) | None
            The linear correction to be applied to the predicted position in 
            the DUT due to a drift in the dut position: 
                 x_corrected=drift_x[0] + Event_number*drift_x[1]
        drift_y: (float,float) | None
            The linear correction to be applied to the predicted position in 
            the DUT due to a drift in the dut position: 
                 y_corrected=drift_y[0] + Event_number*drift_y[1]
        """
        self._needed_treename="fitpoints"
        super(fitpoints,self).__init__(tree)

        # Correct by drift if needed (user entered the correction factors)
        # Store them into the metadata as well... 
        # FIXME: Only make sense few decimals (up to error).
        # FIXME Errors must be included and propagated, or systematically included
        if drift_x:
            self._drift_factor_x=lambda evt: drift_x[1]*evt+drift_x[0]
            self.xPos.at = lambda i: self._xPos[i]+self._drift_factor_x(self._store.GetReadEntry())
        if drift_y:
            self._drift_factor_y=lambda evt: drift_y[1]*evt+drift_y[0]
            self.yPos.at = lambda i: self._yPos[i]+self._drift_factor_y(self._store.GetReadEntry())

    def get_local_position_at(self,idx):
        """Return the local position at the DUT in local coordinates. The
        DUT is being defined by the index (idx), and can be checked at
        `sensorId`.

        Parameters
        ----------
        idx: int
            The index element

        Returns
        -------
        float,float: The x and y where the particle pass through at the DUT 
            and in DUT local coordinates [mm]
        """
        # XXX Be careful in not using the operator [], as the drift 
        #     correction is not propated with this operator       
        return self.xPos.at(idx),fitpoints.yPos.at(idx)

    def get_column_row_at(self,idx,(pitch_x,pitch_y)):
        """Return the local position at the DUT in pixel coordinates (col,row). 
        The DUT is being defined by the index (idx), and can be checked at
        `sensorId`.

        Parameters
        ----------
        idx: int
            The index element
        pitch_x,pitch_y: float,float
            The pitch of a pixel cell

        Returns
        -------
        float,float: The x and y where the particle pass through at the DUT 
            and in DUT local coordinates columns,rows
        """
        return self.xPos.at(idx)/pitch_x,fitpoints.yPos.at(idx)/pitch_y

class tracks(TBTree):
    """Track esti
    """
    def __init__(self,tree):
        """
        """
        self._needed_treename="tracks"
        super(tracks,self).__init__(tree)
                    
                    
    def delegate_hitPattern(self,cluster_instance):
        """Remove the `hitPattern` property link to the tracks 
        TTree). Instead it will delegate the re-creation of the 
        `hitPattern` property to the cluster_instance object
        """
        # override_property needs an extra argument to be used her XXX
        #override_property(self,cluster_instance,'hitPattern')
        def the_getter(self):
            return cluster_instance.modified_hitPattern
        setattr(tracks,"hitPattern",property(the_getter))
        # Keep track of the original hitPattern in the clusters
        cluster_instance._original_hitPattern = self._hitPattern

def tbtree_builder(tree,**args):
    """Instantiate the proper class depending the tree name

    Parameters
    ----------
    tree_name: ROOT.TTree
        The ntuple ttree

    args: Optional arguments (see concrete TBTree classes)

    Returns
    -------
    the proper TBTree daughter instance depending of the tree name
    """
    tree_name = tree.GetName()
    try:
        return TREENAME_TBTree[tree_name](tree,**args)
    except KeyError:
        raise RuntimeError("Invalid tree name: {0}".format(tree_name))


# The correspondance between the ntuple tree name and the 
# TBTree daughter class to be used
TREENAME_TBTree = { 'tracks':tracks, 'fitpoints':fitpoints,
        'rawdata': pixels, 'meashits': hits}
