import setuptools

with open("README.md","r") as fh:
    long_description = fh.read()

setuptools.setup(
        name="sps_tb_eutelizer",
        version="0.1",
        author="Jordi Duarte-Campderros",
        author_email="jorge.duarte.campderros@cern.ch",
        long_description=long_description,
        long_description_content_type="text/markdown",
        url="https://gitlab.cern.ch/sifca/sps-tb-201806-eutel-cfg",
        package_dir={'sps_tb_eutelizer':'python'},
        packages=["sps_tb_eutelizer"],
        scripts=["bin/eutelizer"],
        zip_safe=False,
        classifiers=[
            "Programming Language :: Python :: 2.7",
            "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
            "Operating System :: OS Independent",
            "Development Status :: 4 - Beta",
            ],
        )
