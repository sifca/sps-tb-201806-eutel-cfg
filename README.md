# Test Beam configuration files for EUTelescope and post-processing
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.3748011.svg)](https://doi.org/10.5281/zenodo.3748011)
 
EUTelescope configuration files for test beam data. The configuration files
are test-beam campaign specific, check the tags corresponding to each 
one (see ```Test-beams tags``` section): the last commit of the `master` branch 
does not contain anything but the post-processing scripts.

Remember to create: `output/database`, `output/histograms`,
`output/lcio` and `output/logs` in the output subfolder directory

The main output of the EUTelescope processors is a ntuple containing mostly the fitted
tracks and the measured hits in the DUTs. The ntuple can be post-processed 
by using scripts under the `bin` folder.

## Prerequisites 
 * EUTelescope Docker container: https://github.com/duartej/dockerfiles-eudaqv1/tree/eutelescope
 * Repository installed in the image: https://github.com/duartej/eutelescope
 * EUTelescope and EUDAQ already installed in the docker container.
   * Processors algorithms, available paremeters, could be checked at
     https://github.com/duartej/eutelescope/processors


## Content
 * Post-processing utilities: bin, python folders
 * **tb-cfg**: folder with the test beam campaings configuration and setups. Per each campaing
   it exist an folder with the campaing dependent files/directories:
   * **<config_DUT0[-DUT1-...].cfg**: Configuration parameters for the `jobsub` script
   * **runlist.csv**: Identification of run with geometry file (and other parameters)
   * **gear**: folder containing the geometry description of the setup, telescope,
     REF and DUTs planes. The name convention follows same notation than the config 
     file
 The defined campaings are:
   * **20180724-CERN_SPS-H6B**: CERN SPS test beam at July 2018 [spreadsheet](https://docs.google.com/spreadsheets/d/1iD918r4Utj7OLf3wGLzzkkbgcCxKNQS7kKkEsTQXP9w/edit#gid=0)
   * **20181003-CERN_SPS-H6B**: CERN SPS test beam at first week October 2018 [spreadsheet](https://docs.google.com/spreadsheets/d/10eifb-LYL0gTxuGpVdzf38lw-YJIsFXa1zbbXPHhWgs/edit#gid=0)


   Inside the tb-cfg folder the steering files for the Marlin processor are 
   campaing indepedent: 
   * **steering-templates**: folder containing the steering files to be used as 
         templates for the `jobsub` script. Any parameter of the form 
         `@name@` must exist either in the `.cfg` file or as a `jobsub` option

## Test-beam tags
* 2018-07-25 to 2018-08-06, at SPS (H6B), CERN : [20180724-CERN_SPS-H6B](https://gitlab.cern.ch/sifca/sps-tb-201806-eutel-cfg/tags/20180724-CERN_SPS-H6B)

## EUTelescope processing and configuration
TO BE DOCUMENTED

## Post-processing: DUT characterization
TO BE DOCUMENTED
